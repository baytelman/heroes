# HexaHeroes

Puzzle + Strategy game.

Check out the [Online demo](http://heroesapp.neocities.org) or watch 
[this demo video](https://drive.google.com/file/d/0B_ebGOGbTAmpbDhsLVJrRWxnT28/view?usp=sharing) for v0.1

**In order to automaticly run the repository, first do (assuming you have npm and bower installed):**
```
npm install
bower install
```
After that you only need to run
```
gulp
```
This will monitor your files and open a new tab in chrome, compiling and listening all changes with livereload


**In case you don't want to use gulp just, run ```bower install``` and compile your EcmaScript 6 into Browser-Compatible JS:**
```
babel src -o dist/compiled.js -w
```
Then, open index.html (or any of the other html files)

Check if your changes pass all the tests and increase coverage:
```
istanbul cover dist/compiled.js
```

## Army travels through locked maps (v0.1)

1. Map works like Super Mario 3 / Super Mario World, finishing levels unlocks paths. DONE

2. Visiting towns unlocks powers or give you extra members for your army. DONE

3. Some levels should be too hard and require you to go get stronger, then come back. PENDING

## Battle Roster (v0.1)

1. Player's army starts with only ONE hero. Even when your army grows, you can only bring a limited number of members of your army to fight battles (roster). PENDING

2. Finishing levels should give new members for your army. DONE

3. Finishing levels or leveling up should expand the number of spots of your roster. Initially, you can only bring a couple more characters to battles. As you level up, you can bring more characters to battles. PENDING

4. Before battles (or during the map view) you can open the Roster Editor to select which members of your army will use each of the limited spots to fight your next combat. DONE

## Combat (v0.1)

1. Your pre-defined roster enters a combat. DONE

2. The game auto-plays for both sides, and you see the outcome of the battle. DONE

3. The challenge/puzzle is to choose either WHO to bring to your battles or HOW to level up, traveling the Maps, to get stronger. PENDING