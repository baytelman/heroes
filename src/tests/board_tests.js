
var testTerrainGeneratorSetup = function(obj) {
    obj.g = new HexCoordinate(2,2);
    obj.m = new HexCoordinate(3,6);
    obj.w = new HexCoordinate(2,10);

    let tiles = [
        new TerrainTile(kTerrainGround, obj.g),
        new TerrainTile(kTerrainMountain, obj.m),
        new TerrainTile(kTerrainWater, obj.w),
    ];
    obj.boardDescriptor = new BoardDescriptor(20, 13, kTerrainGround, tiles);
}

tests.push(function testTerrainGenerator() {
    var obj = {};
    testTerrainGeneratorSetup(obj);

    if (obj.boardDescriptor.terrainTiles.length != 3) {
        throw new Error("Board has only anchor tiles");
    }
    obj.boardDescriptor.interpolateTerrain();

    if (obj.boardDescriptor.terrainTiles.length < (20 * 13) / 2) {
        throw new Error("Board has way more tiles");
    }

    let b = new Board(obj.boardDescriptor);
    obj.g.neighborCoordinates().forEach(function(gc) {
        if (b.terrainAtCoordinate(gc) != kTerrainGround) {
            throw new Error("It should be ground!");
        }
    });
    obj.m.neighborCoordinates().forEach(function(mc) {
        if (b.terrainAtCoordinate(mc) != kTerrainMountain) {
            throw new Error("It should be mountain!");
        }
    });
    obj.w.neighborCoordinates().forEach(function(wc) {
        if (b.terrainAtCoordinate(wc) != kTerrainWater) {
            throw new Error("It should be water!");
        }
    });
});
