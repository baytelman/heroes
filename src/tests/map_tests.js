
var testMapSetup = function (obj) {
    var nodes = [
        new MapNode(new SquareCoordinate(0,1), "A"),
        new MapNode(new SquareCoordinate(1,1), "B"),
        new MapNode(new SquareCoordinate(3,0), "C"),
        new MapNode(new SquareCoordinate(2,2), "D"),
        new MapNode(new SquareCoordinate(4,2), "E"),
        new MapNode(new SquareCoordinate(3,3), "F"),
        new MapNode(new SquareCoordinate(5,1), "G"),
        new MapNode(new SquareCoordinate(6,1), "H"),
    ];
    var links = [
        new MapLink([new SquareCoordinate(0,1), new SquareCoordinate(1,1)]), // A->B
        new MapLink([new SquareCoordinate(3,0), new SquareCoordinate(1,1)]), // B->C
        new MapLink([new SquareCoordinate(2,2), new SquareCoordinate(1,1)]), // B->D
        new MapLink([new SquareCoordinate(2,2), new SquareCoordinate(3,3)]), // D->F
        new MapLink([new SquareCoordinate(4,2), new SquareCoordinate(3,3)]), // E->F
        new MapLink([new SquareCoordinate(2,2), new SquareCoordinate(4,2)]), // D->E
        new MapLink([new SquareCoordinate(3,0), new SquareCoordinate(5,1)]), // C->G
        new MapLink([new SquareCoordinate(4,2), new SquareCoordinate(5,1)]), // E->G
        new MapLink([new SquareCoordinate(6,1), new SquareCoordinate(5,1)]), // G->H
    ];
    obj.map = new Map('map_test', 10, 10, nodes, links);
    obj.map.setPlayer(obj.player);
}

tests.push(function testCoordinates() {
    var hex = new HexCoordinate(5,5);
    var hexNei = hex.neighborCoordinates();
    var sq = new SquareCoordinate(5,5);
    var sqNei = sq.neighborCoordinates();

    if (hexNei.length != 6) {
        throw new Error("Wrong number of neighbors for hex");
    }
    if (sqNei.length != 4) {
        throw new Error("Wrong number of neighbors for square");
    }

    hexNei.forEach(function(neighbor) {
        if (hex.distanceTo(neighbor) > 1) {
            throw new Error("Wrong distance for hex neighbor");
        }
    });
    sqNei.forEach(function(neighbor) {
        if (sq.distanceTo(neighbor) > 1) {
            throw new Error("Wrong distance for sq neighbor");
        }
    });
});

tests.push(function testMapBasicTests() {
    var obj = {};
    testArmySetup(obj);
    testMapSetup(obj);
    if (obj.map.unlockedNodes.length != 0) {
        throw new Error("No initial unlocked nodes");
    }
    if (obj.map.accessibleNodes.length != 0) {
        throw new Error("No initial accessible nodes");
    }
});

tests.push(function testMinUnlockTests() {
    var obj = {};
    testArmySetup(obj);
    testMapSetup(obj);
    obj.map.unlock("A");
    if (obj.map.accessibleNodes.length != 2) {
        throw new Error("No initial unlocked nodes");
    }
    if (obj.map.accessibleNodes.indexOf("A") < 0) {
        throw new Error("A should be accessible");
    }
    if (obj.map.accessibleNodes.indexOf("B") < 0) {
        throw new Error("A should be accessible");
    }
});


tests.push(function test2RoutesTests() {
    var obj = {};
    testArmySetup(obj);
    testMapSetup(obj);
    obj.map.unlock("A");
    obj.map.unlock("B");
    obj.map.unlock("C");
    if (obj.map.accessibleNodes.indexOf("G") < 0) {
        throw new Error("G should be accessible");
    }
    if (obj.map.accessibleNodes.indexOf("E") >= 0) {
        throw new Error("E should NOT be accessible");
    }
    testArmySetup(obj);
    testMapSetup(obj);
    obj.map.unlock("A");
    obj.map.unlock("B");
    obj.map.unlock("D");
    if (obj.map.accessibleNodes.indexOf("G") >= 0) {
        throw new Error("G should NOT be accessible");
    }
    obj.map.unlock("E");
    if (obj.map.accessibleNodes.indexOf("G") < 0) {
        throw new Error("G should be accessible");
    }
});
