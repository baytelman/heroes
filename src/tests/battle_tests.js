/**
 * Created by fbaytelm on 8/4/15.
 */


var testBattleSetup = function (obj) {
    var descriptor = new BoardDescriptor(20, 13);
    obj.b = new Board(descriptor);

    obj.f_w = new Character(HeroesWarriorClass);
    obj.f_m = new Character(HeroesMageClass);
    obj.f_a = new Character(HeroesArcherClass);
    obj.f_a.attackRange = function() { return 5 };
    var felipe = new Player("Felipe");

    obj.b.node = new MapNode();
    obj.b.node.map = new Map();
    obj.b.node.map.nodes = [obj.b.node];
    obj.b.node.map.player = felipe;

    obj.b.addPlayer(felipe, 0);
    [obj.f_m, obj.f_w, obj.f_a].forEach(function(c) { felipe.army.addCharacter(c); });
    felipe.roster.addCharacter(obj.f_m, new HexCoordinate(0, 6));
    felipe.roster.addCharacter(obj.f_w, new HexCoordinate(1, 6));
    felipe.roster.addCharacter(obj.f_a, new HexCoordinate(1, 1));
    obj.b.addPlayerAndCharactersFromRoster(felipe.roster);
    // Let's have all Felipe's characters with same health:
    obj.b.characters.forEach(function(character) {
        character.health = 100;
    });

    obj.s_w = new Character(HeroesWarriorClass, new HexCoordinate(5, 1));
    obj.s_m = new Character(HeroesMageClass, new HexCoordinate(4, 1));
    obj.b.node.rosterDescriptor = new Roster();
    obj.b.node.rosterDescriptor.characters = [obj.s_w, obj.s_m];

    obj.b.addEnemyPlayerWithCharactersFromRosterDescriptor(obj.b.node.rosterDescriptor);
};


tests.push(function testNeighbors() {
    var c1 = new HexCoordinate(1, 1);
    var n = c1.neighborCoordinates();

    if (n.length != 6) {
        throw new Error("wrong neighbor count");
    }
    [
        new HexCoordinate(0, 0),
        new HexCoordinate(1, 0),
        new HexCoordinate(0, 1),
        new HexCoordinate(2, 1),
        new HexCoordinate(0, 2),
        new HexCoordinate(1, 2)
    ].forEach(function (c) {
            if (!c.isInArray(n)) {
                throw new Error("missing neighbor: " + c.x + ", " + c.y);
            }
        });
});

tests.push(function testHoodDistances() {
    [new HexCoordinate(5, 5), new HexCoordinate(5, 6)].forEach(function (c1) {
        c1.neighborCoordinates().forEach(function (n) {
            if (c1.distanceTo(n) != 1) {
                throw new Error("wrong distance to neighbor: " + c1.x + ", " + c1.y + " => " + n.x + ", " + n.y);
            }
            n.neighborCoordinates().forEach(function (n2) {
                if (c1.distanceTo(n2) > 2) {
                    throw new Error("wrong distance to neighbor: " + c1.x + ", " + c1.y + " => " + n2.x + ", " + n2.y);
                }
            });
        });
    });

    var c;
    if (new HexCoordinate(5, 5).distanceTo(c = new HexCoordinate(10, 5)) != 5) {
        throw new Error("wrong distance: " + c.x + ", " + c.y);
    }
    if (new HexCoordinate(5, 5).distanceTo(c = new HexCoordinate(10, 0)) != 8) {
        throw new Error("wrong distance: " + c.x + ", " + c.y);
    }
    if (new HexCoordinate(5, 6).distanceTo(c = new HexCoordinate(10, 1)) != 7) {
        throw new Error("wrong distance: " + c.x + ", " + c.y);
    }

});

tests.push(function testIdentifyEnemies() {
    var obj = {};
    testBattleSetup(obj);
    if (obj.b.aliveEnemiesForCharacter(obj.f_w).indexOf(obj.s_w) < 0) {
        throw new Error("wrong enemies");
    }
    if (obj.b.aliveEnemiesForCharacter(obj.f_w).indexOf(obj.f_w) >= 0) {
        throw new Error("wrong enemies");
    }
    if (obj.b.aliveEnemiesForCharacter(obj.s_w).indexOf(obj.s_w) >= 0) {
        throw new Error("wrong enemies");
    }
    if (obj.b.aliveEnemiesForCharacter(obj.s_w).indexOf(obj.f_w) < 0) {
        throw new Error("wrong enemies");
    }
});

tests.push(function testWeakerTargetAvoidTakenSpot() {
    var obj = {};
    testBattleSetup(obj);
    obj.f_w.chooseTargetAmongAliveEnemies();

    // Felipe's warrior should attack Sepehr's HeroesMage, because it's WEAKER
    if (obj.f_w.nextPlayTarget != obj.s_m) {
        throw new Error("wrong target");
    }
    // Felipe's warrior path is a neighbor of HeroesMage, but not where S's warrior is.
    if (!obj.f_w.nextPlayPath.finalCoordinate().isInArray(obj.s_m.coordinate.neighborCoordinates())) {
        throw new Error("wrong path - not a neighbor");
    }
    if (obj.f_w.nextPlayPath.finalCoordinate().isEqualTo(obj.s_w.coordinate)) {
        throw new Error("wrong path - collide with sepher's warrior");
    }
});

tests.push(function testBlockedPath() {
    var obj = {};
    testBattleSetup(obj);
    for (var i = 0; i < obj.b.height - 4; i++) {
        var new_w = new Character(HeroesWarriorClass);
        obj.b.addCharacter(obj.s_w.player, new_w, new HexCoordinate(16, i));
    }
    obj.f_w.chooseTargetAmongAliveEnemies()

    // Felipe's warrior should attack Sepehr's HeroesMage, because it's WEAKER
    if (obj.f_w.nextPlayTarget != obj.s_m) {
        throw new Error("wrong target");
    }

    // Felipe's warrior try to pass the warrior barrier
    if (!new HexCoordinate(16, obj.b.height - 4).isInArray(obj.f_w.nextPlayPath.steps)) {
        throw new Error("not trying to attack");
    }
});

tests.push(function testRangedAttacks() {
    var obj = {};
    testBattleSetup(obj);
    obj.f_w.chooseTargetAmongAliveEnemies()
    obj.f_a.chooseTargetAmongAliveEnemies()
    if (BattleHelper.characterNextPlayStepsCost(obj.f_w) <= BattleHelper.characterNextPlayStepsCost(obj.f_a)) {
        throw new Error("archer needs less steps");
    }
});

tests.push(function testTurnOrder() {
    var obj = {};
    testBattleSetup(obj);

    // Let's speed up these 5 characters.
    var speed = 10;
    var characters = [obj.f_w, obj.s_w, obj.f_m, obj.s_m, obj.f_a];
    characters.forEach(function (character) {
        var s = speed--;
        character.speed = function() { return s };
    });

    var nextCharacters = obj.b.nextCharactersToPlay();
    var fast_characters_are_first = true;
    nextCharacters.forEach(function (element, index) {
        fast_characters_are_first = fast_characters_are_first && (element === nextCharacters[index]);
    });

    if (!fast_characters_are_first) {
        throw new Error("fast_characters_are_first");
    }

    var firstToPlay = obj.b.nextCharacterMovesAndAttacks();

    if (firstToPlay.character != obj.f_w) {
        throw new Error("fast warrior should be first");
    }

    nextCharacters = obj.b.nextCharactersToPlay();
    characters.shift();
    fast_characters_are_first = true;
    characters.forEach(function (element, index) {
        fast_characters_are_first = fast_characters_are_first && (element === nextCharacters[index]);
    });

    if (!fast_characters_are_first) {
        throw new Error("next after f warrior should be in line");
    }
});

tests.push(function testAttackNearHeroesWarrior() {
    var obj = {};
    testBattleSetup(obj);
    // Put enemy directly in front of him
    var x = obj.f_w.coordinate.x;
    obj.s_w.coordinate = new HexCoordinate(x + 2, obj.f_w.coordinate.y);
    // Let's force him to attack first
    obj.f_w.speed = function () { return 10; };
    obj.s_w.speed = function () { return 9; };

    obj.f_m.health = obj.s_m.health;

    var play = obj.b.nextCharacterMovesAndAttacks();
    if (play.character != obj.f_w) {
        throw new Error("first character to play should be f w");
    }
    if (play.attackResults[0].target != obj.s_w) {
        throw new Error("first character should attack s w");
    }
    if (obj.f_w.coordinate.x != x + 1) {
        throw new Error("first character should have moved by s w side");
    }
    if (play.attackResults[0].damage > 15 ||
        play.attackResults[0].damage < 5) {
        throw new Error("damage beyond range");
    }
    if (obj.s_w.health != obj.s_w.maxHealth() - play.attackResults[0].damage) {
        throw new Error("s w should have receive damage");
    }

    x = obj.s_w.coordinate.x;
    play = obj.b.nextCharacterMovesAndAttacks();
    if (play.character != obj.s_w) {
        throw new Error("now should be s w's turn");
    }
    if (play.attackResults[0].target != obj.f_m) {
        throw new Error("attacking f mage, cos he is weaker and in range");
    }
    if (!obj.s_w.coordinate.isInArray(obj.f_m.coordinate.neighborCoordinates())) {
        throw new Error("s w should be near f m");
    }

    for (var i = 0; i < 30; i++) {
        play = obj.b.nextCharacterMovesAndAttacks();
    }
});

tests.push(function testMovingOnTerrain() {
    var obj = {};
    testBattleSetup(obj);

    let cGround = obj.f_w.costToMoveOnTerrain(kTerrainGround);
    let cWater = obj.f_w.costToMoveOnTerrain(kTerrainWater);
    let cMountain = obj.f_w.costToMoveOnTerrain(kTerrainMountain);

    if (cGround >= cMountain || cMountain >= cWater) {
        throw new Error("Cost to move should be Ground < Mountain < Water");
    }
});
