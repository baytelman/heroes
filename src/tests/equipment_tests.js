function testEquipmentSetup(obj) {
    testArmySetup(obj);
    obj.sword = new HeroesSwordOfSpeed();
    obj.morePowerfulSword = new HeroesSwordOfSpeed();
    obj.morePowerfulSword.level = 3;

    obj.player.earnItem(obj.sword);
    obj.bow = new HeroesBowOfDistance();
}

tests.push(function testEquipementBasicTests() {
    let obj = {};
    testEquipmentSetup(obj);
    if (obj.player.getAllEquipment().length != 1) {
        throw new Error("Player starts with no equipment");
    }
});

tests.push(function testCanEquipItem() {
    let obj = {};
    testEquipmentSetup(obj);

    /* Something I don't have */
    if (obj.hero.canEquip(obj.bow)) { throw Error("Player doesnt have a bow"); }
    try {
        obj.hero.equip(obj.bow);
        throw Error("Player doesnt have a bow");
    } catch (e) {
        if (!(e instanceof ItemNotAvailableError)) {
            throw e;
        }
    }

    obj.player.earnItem(obj.bow);
    if (obj.hero.canEquip(obj.bow)) { throw Error("Warrior cannot use bows"); }
    try {
        obj.hero.equip(obj.bow);
        throw Error("Warrior cannot use bows");
    } catch (e) {
        if (!(e instanceof ItemNotCompatibleError)) {
            throw e;
        }
    }

    if (obj.hero.canEquip(obj.bow)) { throw Error("Archer is not the hero"); }
    try {
        obj.a1.equip(obj.bow);
        throw Error("Archer is not the hero");
    } catch (e) {
        if (!(e instanceof NonHeroCannotEquipError)) {
            throw e;
        }
    }

    if (!obj.hero.canEquip(obj.sword)) { throw Error("Hero should be able to equip sword"); }
    obj.hero.equip(obj.sword);

    /* Hero already equipped something */
    obj.player.earnItem(obj.morePowerfulSword);
    if (obj.hero.canEquip(obj.sword)) { throw Error("Already wearing the other sword"); }
    try {
        obj.hero.equip(obj.morePowerfulSword);
        throw Error("Already wearing the other sword");
    } catch (e) {
        if (!(e instanceof HeroAlreadyEquippedError)) {
            throw e;
        }
    }

    obj.hero.unequipAll();
    if (!obj.hero.canEquip(obj.sword)) { throw Error("Hero should be able to equip powerful sword"); }
    obj.hero.equip(obj.morePowerfulSword);
});

tests.push(function testEffectsOfItems() {
    let obj = {};
    testEquipmentSetup(obj);

    if (Effect.modifiedMoveRangeForCharacter(obj.hero) != obj.hero.moveRange()) {
        throw new Error("No equipment to affect hero");
    }

    obj.hero.equip(obj.sword);

    if (Effect.modifiedMoveRangeForCharacter(obj.hero) <= obj.hero.moveRange()) {
        throw new Error("Sword of Speed should affect moveRange");
    }
    if (Effect.modifiedAttackRangeForCharacter(obj.hero) != obj.hero.attackRange()) {
        throw new Error("Sword of Speed should NOT affect attackRange");
    }
});

tests.push(function testItemLevelEffects() {
    let obj = {};
    testEquipmentSetup(obj);

    obj.hero.equip(obj.sword);
    let weakerSwordRange = Effect.modifiedMoveRangeForCharacter(obj.hero);
    obj.hero.unequipAll();

    obj.player.earnItem(obj.morePowerfulSword);
    obj.hero.equip(obj.morePowerfulSword);
    let powerfulSwordRange = Effect.modifiedMoveRangeForCharacter(obj.hero);

    if (weakerSwordRange >= powerfulSwordRange) {
        throw new Error("Sword of Speed 3 should make you move faster than Sword of Speed 1");
    }
});


tests.push(function testEquippedItemAffectsBattle() {

    let coordinateAfterOneTurn = {}
    let f = false;
    let t = true;
    [f, t].forEach(function(useItem) {
        let obj = {};
        testEquipmentSetup(obj);
        testBattleSetup(obj);

        /* Ensure f_w is the firt */
        obj.f_w = 1000;
        var nextCharacters = obj.b.nextCharactersToPlay();

        /* Let's say this guy is the hero, so he can wear stuff */
        let hero = nextCharacters[0];
        hero.player.army.hero = hero;

        if (useItem) {
            /* Let's give him a sword */
            hero.player.earnItem(obj.morePowerfulSword);
            hero.equip(obj.morePowerfulSword);
        }
        var play = obj.b.nextCharacterMovesAndAttacks();
        coordinateAfterOneTurn[useItem] = hero.coordinate;
    });
    if (coordinateAfterOneTurn[false].x >= coordinateAfterOneTurn[true].x) {
        throw new Error("With sword of speed, warrior should get closer to his enemies");
    }
});
