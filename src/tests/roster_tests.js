/**
 * Created by fbaytelm on 8/11/15.
 */

var testArmySetup = function (obj) {
    obj.player= new Player("Felipe", 0);
    obj.hero = new Character(HeroesWarriorClass);
    obj.hero.earnExperience(300);
    obj.m = new Character(HeroesMageClass);
    obj.a1 = new Character(HeroesArcherClass);
    obj.a2 = new Character(HeroesArcherClass);
    obj.w1 = new Character(HeroesWarriorClass);
    obj.w2 = new Character(HeroesWarriorClass);

    obj.player.army.setHero(obj.hero);
    obj.player.army.addCharacter(obj.m);
    obj.player.army.addCharacter(obj.a1);
    obj.player.army.addCharacter(obj.a2);
    obj.player.army.addCharacter(obj.w1);
    obj.player.army.addCharacter(obj.w2);
};

var testArmyLevelUp = function (obj) {
    obj.hero = new Character(HeroesWarriorClass);
    let mh = obj.hero.maxHealth;
    obj.hero.earnExperience(300);
    if (obj.hero.getLevel() < 3) {
        throw new Error("HeroesWarrior level should be 3");
    }
    if (obj.hero.maxHealth <= mh) {
        throw new Error("HeroesWarrior max health is more");
    }
}
var testRosterSetup = function (obj) {
    obj.player.roster.addCharacter(obj.m, new HexCoordinate(-1, 0)); // Left of the hero
    obj.player.roster.addCharacter(obj.a1, new HexCoordinate(0, -1)); // Below the hero
    obj.player.roster.addCharacter(obj.a2, new HexCoordinate(0, 1)); // Above of the hero
};

tests.push(function testCoreArmy() {
    var obj = {};
    testArmySetup(obj);

    if (obj.player.army.characters.indexOf(obj.hero) < 0) {
        throw new Error("Army should always include its hero");
    }
    if (obj.player.roster.activeCharacters().length != 1 || obj.player.roster.activeCharacters().indexOf(obj.hero) < 0) {
        throw new Error("Roster should always include the player's hero");
    }
});

tests.push(function testHealArmy() {
    var obj = {};
    testArmySetup(obj);
    obj.m.receiveDamage(obj.m.maxHealth());
    if (obj.m.health != 0) {
        throw new Error("HeroesMage should be dead");
    }
    obj.player.updateTime(1000);
    if (obj.m.health != obj.m.maxHealth()) {
        throw new Error("HeroesMage should be fully healed");
    }
});

tests.push(function testCannotAddForeignersRoster() {
    var obj = {};
    testArmySetup(obj);

    try {
        obj.player.roster.addCharacter(new Character(HeroesWarriorClass, new HexCoordinate(1, 1)));
        obj.player.roster.checkConsistency();
        throw new Error("Cannot add foreign to roster");
    } catch (e) {
        if (!(e instanceof RosterError)) {
            throw e;
        }
    }
});

tests.push(function testCannotAddAboveExistingCharacter() {
    var obj = {};
    testArmySetup(obj);

    try {
        obj.player.roster.addCharacter(obj.a1, new HexCoordinate(0, 0));
        obj.player.roster.checkConsistency();
        throw new Error("Cannot overlap hero");
    } catch (e) {
        if (!(e instanceof RosterError)) {
            throw e;
        }
    }
});

tests.push(function testCoreRoster() {
    var obj = {};
    testArmySetup(obj);
    if (obj.m.isInRoster()) {
        throw new Error("Warrior should NOT be in roster");
    }

    testRosterSetup(obj);
    if (!obj.m.isInRoster()) {
        throw new Error("Warrior SHOULD be in roster");
    }
});

tests.push(function testRemoveFromRoster() {
    var obj = {};
    testArmySetup(obj);
    testRosterSetup(obj);
    var coord = obj.m.initialCoordinate;
    var count = obj.player.roster.activeCharacters().length;
    obj.player.roster.removeCharacterWithUniqueName(obj.hero.uniqueName);
    if (obj.player.roster.activeCharacters().length != count) {
        throw new Error("Cannot remove hero");
    }
    obj.player.roster.removeCharacterWithUniqueName(obj.m.uniqueName);
    if (obj.player.roster.activeCharacters().length != count - 1) {
        throw new Error("Didn't remove mage");
    }
    obj.player.roster.addCharacterWithUniqueName(obj.m.uniqueName, coord);
    if (obj.player.roster.activeCharacters().length != count) {
        throw new Error("Re-added the mage");
    }
});

tests.push(function testCannotAddAlreadyAddedCharacter() {
    var obj = {};
    testArmySetup(obj);
    testRosterSetup(obj);

    try {
        obj.player.roster.addCharacter(obj.a1, new HexCoordinate(5, 5));
        throw new Error("Cannot add existing");
    } catch (e) {
        if (!(e instanceof RosterError)) {
            throw e;
        }
    }
});
