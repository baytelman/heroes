/**
* Created by fbaytelm on 8/17/15.
*/

tests.push(function testEncodePlayer() {
    var pn = "player_name";
    var pa = 2;
    var p = new Player(pn, pa);
    var ph = new Character(HeroesWarriorClass);
    p.army.setHero(ph);
    var d = JSON.parse(p.asJSON());
    if (d.name !== pn) {
        throw new Error("Wrong player encoding");
    }
});

tests.push(function testEncodeDecodePlayerNodes() {
    var obj = {};
    testArmySetup(obj);
    testMapSetup(obj);
    var nid_a = "A";
    var nid_b = "B";
    obj.player.unlockedMapNodeWithId(obj.map.id, nid_a);
    obj.player.unlockedMapNodeWithId(obj.map.id, nid_b);

    var p2 = Player.playerWithJSON(obj.player.asJSON());
    obj.player = p2;
    testMapSetup(obj);
    if (!obj.map.nodeWithId(nid_a).isUnlocked()) {
        throw new Error("Wrong player decoding (map nodes)");
    }
    var count = 0;
    for (var key in p2.unlockedNodes[obj.map.id]) { count++; }
    if (count !== 2) {
        throw new Error("Wrong player decoding (map nodes)");
    }
});
