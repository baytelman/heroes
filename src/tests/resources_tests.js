
tests.push(function testResourcesBasicTests() {
    let obj = {};
    testArmySetup(obj);
    if (obj.player.getResourceAmountForType(kResourceGold) > 0) {
        throw new Error("Player starts with no resources");
    }
    obj.player.earnResource(Resource.gold(100));
    if (obj.player.getResourceAmountForType(kResourceGold) != 100) {
        throw new Error("Player just earned 100");
    }

    let moreGold = Resource.gold(100).resourceWithMultiplier(2);
    if (moreGold.amount != 200) {
        throw new Error("Wrong resourceWithMultiplier output");
    }
});

tests.push(function testHealTests() {
    let obj = {};
    testArmySetup(obj);

    let hca = new HealCharacterAction(obj.m);
    let c = hca.costs();
    obj.m.receiveDamage(obj.m.maxHealth());
    if (c.length != 1 || c[0].type != kResourceGold || c[0].amount <= 0) {
        throw new Error("Wrong cost to heal");
    }
    if (Resource.playerCanAfford(obj.player, c)) {
        throw new Error("Player cannot afford");
    }
    try {
        hca.executeForPlayer(obj.player);
        throw Error("Player cannot afford");
    } catch (e) {
        if (!(e instanceof InsuficientResourcesError)) {
            throw e;
        }
    }

    obj.player.earnResources(c);
    hca.executeForPlayer(obj.player);
    if (obj.m.health < obj.m.maxHealth()) {
        throw new Error("Healing action did not take place");
    }

    if (Resource.playerCanAfford(obj.player, c)) {
        throw new Error("Player cannot afford it twice");
    }
});

tests.push(function testHealUnavailableTests() {
    let obj = {};
    testArmySetup(obj);

    let hca = new HealCharacterAction(obj.m);
    let c = hca.costs();
    obj.player.earnResources(c);

    if (hca.isAvailable()) {
        throw new Error("Healing shouldnt be available to healed character");
    }
    try {
        hca.executeForPlayer(obj.player);
        throw Error("Healing is unavailable");
    } catch (e) {
        if (!(e instanceof UnavailableActionError)) {
            e;
        }
    }
});

tests.push(function testHealAll() {
    let obj = {};
    testArmySetup(obj);
    obj.hero.recoverAllHealth();
    obj.m.receiveDamage(obj.m.maxHealth());
    obj.w1.receiveDamage(obj.w1.maxHealth());

    let haa = new HealArmyAction(obj.player.army);
    let c = haa.costs();
    obj.player.earnResources(c);

    if (c[0].amount != obj.w1.healAction.costs()[0].amount + obj.m.healAction.costs()[0].amount) {
        throw new Error("Army healing should cost the added of all characters");
    }
    haa.executeForPlayer(obj.player);
    if (obj.m.health < obj.m.maxHealth() || obj.w1.health < obj.w1.maxHealth()) {
        throw new Error("Healing army action did not take place");
    }
});

tests.push(function testUpgradeWarrior() {
    let obj = {};
    testArmySetup(obj);
    obj.w1.receiveDamage(obj.w1.maxHealth());

    let uca = obj.w1.characterClass.upgradeCharacterLevelAction();
    let c = uca.costs();
    obj.player.earnResources(c);

    if (!uca.isAffordable(obj.player)) {
        throw new Error("Warrior's upgrade character costs");
    }

});
