/**
 * Created by fbaytelm on 8/9/15.
 */

/**
 * Represents a coordinate in the board.
 * It will represent either a character's current location or a seriers of steps a character will move through.
 */
class SquareCoordinate {
    constructor(x, y) {
        if (x instanceof Object) {
            return this.constructor(x.x, x.y);
        }
        this.x = x;
        this.y = y;
    }
    copy() {
        return new SquareCoordinate(this.x, this.y);
    }

    /// Debug representation
    toString() {
        return "<{0}, {1}>".format(this.x, this.y);
    }
    /// Debug representation
    toJString() {
        var x = this.x >= 0? this.x : 'm' + Math.abs(this.x);
        var y = this.y >= 0? this.y : 'm' + Math.abs(this.y);
        return "_{0}_{1}_".format(x, y);
    }

    isEqualTo(anotherCoordinate) {
        return this.x == anotherCoordinate.x && this.y == anotherCoordinate.y;
    }

    /**
     * Calculate the minimun distance between 2 coordinates
     * @param {HexCoordinate} dest
     * @returns {number} Tile-distance between the 2 coordinates.
     */
    distanceTo(dest) {
        var vDist = Math.abs(this.y - dest.y);
        var hDist = Math.abs(this.x - dest.x);
        return vDist + hDist;
    }

    /**
     * List of neighbor tiles (up to 6). They might be out of the board
     * @returns {Array.<HexCoordinate>}
     */
    neighborCoordinates() {
        var n = [];

        n.push(new SquareCoordinate(this.x - 1, this.y));
        n.push(new SquareCoordinate(this.x + 1, this.y));
        n.push(new SquareCoordinate(this.x, this.y + 1));
        n.push(new SquareCoordinate(this.x, this.y - 1));

        return n;
    }

    // returns BOOL if coordinate is in array
    isInArray(array) {
        var c = this;
        var found = false;
        array.forEach(function (nn) {
            if (nn.isEqualTo(c)) {
                found = true;
            }
        });
        return found;
    }
}


/**
 * Represents a coordinate in the board.
 * It will represent either a character's current location or a seriers of steps a character will move through.
 */
class HexCoordinate extends SquareCoordinate {
    /**
     * Calculate the minimun distance between 2 coordinates
     * @param {HexCoordinate} dest
     * @returns {number} Tile-distance between the 2 coordinates.
     */
    distanceTo(dest) {
        var vDist = Math.abs(this.y - dest.y);
        var xDist = Math.abs(this.x - dest.x);
        if ((this.y % 2 == 0 && this.x > dest.x) || (Math.abs(this.y % 2) == 1 && this.x < dest.x))
            vDist += Math.max(0, xDist - Math.floor(vDist / 2.0));
        else
            vDist += Math.max(0, xDist - Math.ceil(vDist / 2.0));

        return vDist;
    }


    /**
     * List of neighbor tiles (up to 6). They might be out of the board
     * @returns {Array.<HexCoordinate>}
     */
    neighborCoordinates(distance=1) {
        var n = [];

        for (var x = this.x - 2 * distance; x <= this.x + 2 * distance; x++) {
            for (var y = this.y - 2 * distance; y <= this.y + 2 * distance; y++) {
                var c = new HexCoordinate(x, y);
                var d = c.distanceTo(this);
                if (d > 0 && d <= distance) {
                    n.push(c);
                }
            }
        }
        return n;
    }
}
