'use strict'

class EquipmentError extends Error {
}
class HeroAlreadyEquippedError extends EquipmentError {
}
class ItemNotAvailableError extends EquipmentError {
}
class ItemNotCompatibleError extends EquipmentError {
}
class NonHeroCannotEquipError extends EquipmentError {
}

class Effect {
    constructor() {
        this.level = 1;
    }
    modifyAttackRange(original) {
        return original;
    }
    modifyMoveRange(original) {
        return original;
    }
    modifySpeed(original) {
        return original;
    }

    static modifiedMoveRangeForCharacter(character) {
        let original = character.moveRange();
        let modified = original;
        character.getAllEquipment().forEach(function(item) {
            modified = item.modifyMoveRange(modified);
        })
        return modified;
    }
    static modifiedAttackRangeForCharacter(character) {
        let original = character.attackRange();
        let modified = original;
        character.getAllEquipment().forEach(function(item) {
            modified = item.modifyAttackRange(modified);
        })
        return modified;
    }
    static modifiedSpeedForCharacter(character) {
        let original = character.speed();
        let modified = original;
        character.getAllEquipment().forEach(function(item) {
            modified = item.modifySpeed(modified);
        })
        return modified;
    }
}

const kEquipmentWeapon = 'weapon';
const kEquipmentArmor = 'armor';
const kEquipmentDefault = kEquipmentWeapon;

class BaseItem extends Effect {
    constructor() {
        super();
        this.type = kEquipmentDefault;
        this.allowedClasses = null; // All
    }
}

class HeroesSwordOfSpeed extends BaseItem {
    modifyMoveRange(original) {
        return original + this.level;
    }
    modifySpeed(original) {
        return original + this.level * 3
    }
}

class HeroesBowOfDistance extends BaseItem {
    constructor() {
        super();
        this.allowedClasses = ["HeroesArcherClass"];
    }
}
