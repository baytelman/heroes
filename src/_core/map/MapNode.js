const kRewardReward = 'reward';
const kRewardResources = 'resources';

class MapNode {
    constructor(coordinate, id, boardDescriptor) {
        this.coordinate = coordinate;
        this.id = id;

        this.itemRewardClass = null;
        this.rewardClasses = null;
        this.rewardDelay = null;
        this.maxRewardCount = 1;

        this.resourcesGeneration = [];
        this.boardDescriptor = null;
        this.rosterDescriptor = null;
        this.rosterBaseExperience = 0;
    }
    getRosterDescriptor() {
        if (typeof this.rosterDescriptor === 'string') {
            this.rosterDescriptor = Roster.rosterWithURL(this.rosterDescriptor);
        }
        return this.rosterDescriptor;
    }
    getBoardDescriptor() {
        if (typeof this.boardDescriptor === 'string') {
            this.boardDescriptor = BoardDescriptor.boardDescriptorWithURL(this.boardDescriptor);
        }
        return this.boardDescriptor;
    }
    isUnlocked() {
        return this.map.unlockedNodes.indexOf(this.id) >= 0;
    }
    isAccessible() {
        return this.map.accessibleNodes.indexOf(this.id) >= 0;
    }
    rewardPrefix() {
        return '-reward' + this.id;
    }
    totalExperienceForPlayer(player) {
        let sum = 0;
        this.getRosterDescriptor().characters.forEach(function(character) {
            sum += character.experienceForPlayer(player);
        });
        return sum;
    }
    startBattle() {
        var b = new Board(this);
        b.addEnemyPlayerWithCharactersFromRosterDescriptor(this.getRosterDescriptor(), this.rosterBaseExperience);
        return b;
    }
    rewardRoster(player, characters) {
        let levelUps = [];
        let perUnit = this.totalExperienceForPlayer(player) / characters.length;
        characters.forEach(function(character) {
            if (character.earnExperience(perUnit)) {
                levelUps.push(character);
            }
        });
        return levelUps;
    }
    rewardPlayer(player) {
        let rewards = [];
        let node = this;
        if (this.rewardClasses) {
            this.rewardClasses.forEach(function (rewardClassName) {
                var rewardClass = eval(rewardClassName);
                var rewardInstance = new rewardClass();
                if (rewardInstance instanceof BaseCharacterClass) {
                    var character = new Character(rewardClass);
                    character.uniqueName = node.rewardPrefix()
                    + '-' + player.uniqueName
                    + '_' + (player.army.characters.length + 1);
                    player.army.addCharacter(character);
                    rewards.push(character);
                } else if (rewardInstance instanceof BaseItem) {
                    player.earnItem(rewardInstance);
                    rewards.push(rewardInstance);
                }
            });
        }
        return rewards;
    }
    waitUntilCanRewardPlayer() {
        if (this.rewardClasses === null) {
            return null;
        }
        if (this.maxRewardCount && this.map.player.rewardCoundForMapNodeWithId(this.map.id, this.id, kRewardReward) >= this.maxRewardCount) {
            return null;
        }
        var wait = 0;
        if (this.rewardDelay) {
            var elapsed = this.map.player.timeElapsedSinceLastRewardForMapNodeWithId(this.map.id, this.id, kRewardReward);
            if (elapsed !== null) {
                wait = this.rewardDelay - elapsed;
                if (wait < 0) {
                    wait = 0;
                }
            }
        }
        return wait;
    }
    resourceRewardsWithTime() {
        if (!this.resourcesGeneration) {
            return [];
        }
        var elapsed = this.map.player.timeElapsedSinceLastRewardForMapNodeWithId(this.map.id, this.id, kRewardResources);
        if (!elapsed && elapsed <= 0) {
            return [];
        }
        return Resource.resourcesWithMultiplier(this.resourcesGeneration, elapsed);
    }
}
