/**
* Created by fbaytelm on 8/9/15.
*/

class MapLink {
    constructor(coordinates, id) {
        this.coordinates = coordinates;
    }
    isAccessible() {
        return this.map.nodeWithCoordinate(this.start()).isUnlocked()
        || this.map.nodeWithCoordinate(this.end()).isUnlocked();
    }
    connects(node) {
        for (var i = 0; i < this.coordinates.length; i++) {
            if (this.coordinates[i].isEqualTo(node.coordinate)) {
                return true;
            }
        }
        return false;
    }
    start() {
        return this.coordinates[0].copy();
    }
    end() {
        return this.coordinates[this.coordinates.length - 1].copy();
    }
    tiles(zoom) {
        if (!zoom) {
            zoom = 1;
        }
        var result = [];
        var incrX = 1;
        var incrY = 1;
        var first = this.start();
        first.x *= zoom;
        first.y *= zoom;
        this.coordinates.slice(1).forEach(function(tile) {
            var last = tile.copy();
            last.x *= zoom;
            last.y *= zoom;
            if (first.x > last.x) {
                incrX = -1;
            }
            if (first.y > last.y) {
                incrY = -1;
            }
            if (result.length > 0) {
                result.pop();
            }
            for (var c = first; ! first.isEqualTo(last);) {
                result.push(first.copy());
                if (first.x != last.x) {
                    first.x += incrX;
                }
                if (first.y != last.y) {
                    first.y += incrY;
                }
            }
            result.push(last.copy());
        });
        return result;
    }
}
