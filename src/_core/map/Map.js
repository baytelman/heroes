
class Map {
    static sharedMapDictionary() {
        if (!this._sharedMapDictionary) {
            var m = setupDemoMap();
            this._sharedMapDictionary = {};
            this._sharedMapDictionary[m.id] = m;
        }
        return this._sharedMapDictionary;
    }
    constructor(id, width, height, nodes=[], links=[]) {
        this.id = id;

        this.width = width;
        this.height = height;
        this.nodes = nodes;
        this.links = links;
        this.unlockedNodes = [];
        this.accessibleNodes = [];

        this._setupChildrenRefereces();
    }
    static mapWithURL(jsonUrl) {
        let json = loadTextFileSync(jsonUrl);
        return Map.mapWithJSON(json);
    }
    static mapWithJSON(json) {
        let map = CyclicalJSON.cyclicalObjectWithJSON(json, function(m) {
            m._setupChildrenRefereces();
        });
        return map;
    }
    _setupChildrenRefereces() {
        var map = this;
        if (this.nodes) {
            this.nodes.forEach(function(node) { node.map = map});
        }
        if (this.links) {
            this.links.forEach(function(node) { node.map = map});
        }
    }
    setPlayer(player) {
        this.player = player;
        if (player) {
            var map = this;
            let unlockedNodesForPlayer = this.player.unlockedNodeIdsForMapWithId(this.id);
            for (let nodeId in unlockedNodesForPlayer) {
                map.unlock(nodeId);
            }
        }
    }
    unlock(nodeId) {
        var map = this;
        var node = this.nodeWithId(nodeId);
        if (node) {
            this.accessNode(node.id);

            if (this.unlockedNodes.indexOf(nodeId) < 0) {
                this.unlockedNodes.push(nodeId);
            }
            this.links.forEach(function(link) {
                if (link.connects(node)) {
                    map.accessNode(map.nodeWithCoordinate(link.start()).id);
                    map.accessNode(map.nodeWithCoordinate(link.end()).id);
                }
            });
        }
    }
    accessNode(nodeId) {
        var node = this.nodeWithId(nodeId);
        if (node) {
            if (this.accessibleNodes.indexOf(nodeId) < 0) {
                this.accessibleNodes.push(nodeId);
            }
        }
    }
    nodeWithId(nodeId) {
        for (var i = 0; i < this.nodes.length; i++) {
            if (this.nodes[i].id == nodeId) {
                return this.nodes[i];
            }
        }
        return null;
    }
    nodeWithCoordinate(coordinate) {
        for (var i = 0; i < this.nodes.length; i++) {
            if (this.nodes[i].coordinate.isEqualTo(coordinate)) {
                return this.nodes[i];
            }
        }
        return null;
    }
}
