'use strict'

class HeroesWarriorClass extends BaseCharacterClass {
    getName() {
        return "Warrior";
    }
    chooseTargetAmongAliveEnemies() {
        BattleHelper.chooseWeakestEnemyPrioritizingWithinRange(this.character);
    }
    upgradeCharacterClassActions() {
        return [
            new UpgradeCharacterTypeAction(this.character, [
                Resource.wood(5),
                Resource.gold(10)
            ], HeroesArcherClass),
            new UpgradeCharacterTypeAction(this.character, [
                Resource.jewel(5),
                Resource.gold(30)
            ], HeroesMageClass)
        ];
    }
}

class HeroesArcherClass extends HeroesWarriorClass {
    calculateAttackDamage() {
        let center = 5 + this.character.getLevel() * 2;
        let random = 8 + this.character.getLevel();
        return randomWithCenterAndSpread(center, random);
    }
    attackRange() {
        if (this.character.getLevel() > 9) {
            return 4;
        }
        if (this.character.getLevel() > 7) {
            return 3;
        }
        return 5;
    }
    maxHealth() {
        return Math.round(67 * (0.9 + this.character.getLevel()/10.0));
    }
    getName() {
        return "Archer";
    }
    upgradeCharacterLevelAction() {
        return new UpgradeCharacterLevelAction(this.character, [
            Resource.human(1),
            Resource.wood(4),
            Resource.gold(9)
        ]);
    }
    upgradeCharacterClassActions() {
        return [];
    }
}

class HeroesMageClass extends HeroesWarriorClass {
    attackRange() {
        if (this.character.getLevel() > 5) {
            return 4;
        }
        if (this.character.getLevel() > 5) {
            return 3;
        }
        return 2;
    }
    moveRange() {
        return 3;
    }
    maxHealth() {
        return Math.round(30 * (0.9 + this.character.getLevel()*this.character.getLevel()/10.0));
    }
    calculateAttackDamage() {
        let center = 8 + this.character.getLevel() * 3;
        let random = 14 + this.character.getLevel() * 2;
        return randomWithCenterAndSpread(center, random);
    }
    getName() {
        return "Mage";
    }
    attack() {
        var attacks = [];
        if (this.character.isTargetInRange()) {
            var damageInCenter = this.calculateAttackDamage();
            var enemies = this.character.board.aliveEnemiesForCharacter(this.character);
            var mainTarget = this.character.nextPlayTarget;
            enemies.forEach(function (enemy) {
                var d = enemy.coordinate.distanceTo(mainTarget.coordinate);
                if (d < 2) {
                    var damage = damageInCenter / (d + 1);
                    enemy.receiveDamage(damage);
                    attacks.push(new AttackResult(enemy, damage));
                }
            });
        }
        return attacks;
    }
    upgradeCharacterLevelAction() {
        return new UpgradeCharacterLevelAction(this.character, [
            Resource.human(2),
            Resource.jewel(4),
            Resource.gold(25)
        ]);
    }
    upgradeCharacterClassActions() {
        return [
            new UpgradeCharacterTypeAction(this.character, [
                Resource.jewel(25),
                Resource.gold(50)
            ], HeroesDragonClass)
        ];
    }
}

class HeroesDragonClass extends HeroesMageClass {
    getName() {
        return "Dragon";
    }
    costToMoveOnTerrain(terrain) {
        return 1;
    }
    moveRange() {
        return 8;
    }
    canFlyOverOtherCharacters() {
        return true;
    }
    upgradeCharacterLevelAction() {
        return new UpgradeCharacterLevelAction(this.character, [
            Resource.human(3),
            Resource.jewel(20),
            Resource.gold(40)
        ]);
    }
    upgradeCharacterClassActions() {
        return [];
    }
}
