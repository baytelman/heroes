'use strict'

/**
* Abstract character class.
*/

class Character {
    constructor(characterClass, initialCoordinate) {
        this.uniqueName = null;

        this.player = null;
        this.board = null;

        this.initialCoordinate = initialCoordinate;
        this.experience = 0;

        this.equipment = {};

        if (characterClass) {
            this.setClass(characterClass);
            this.recoverAllHealth();
        }
    }

    setClass(characterClass) {
        this.characterClass = new characterClass(this);
    }

    getShortName() {
        return this.characterClass.getName()[0] + this.getLevel();
    }

    getName() {
        return this.characterClass.getName() + " (" + this.getLevel() + ")";
    }

    maxHealth() {
        return this.characterClass.maxHealth()
    }

    healRate() {
        return this.characterClass.healRate();
    }

    chooseTargetAmongAliveEnemies() {
        return this.characterClass.chooseTargetAmongAliveEnemies();
    }

    costToMoveOnTerrain(terrain) {
        return this.characterClass.costToMoveOnTerrain(terrain);
    }

    canLandOnTerrain(terrain) {
        return this.characterClass.canLandOnTerrain(terrain);
    }

    moveRange() {
        return this.characterClass.moveRange();
    }

    speed() {
        return this.characterClass.speed();
    }

    attackRange() {
        return this.characterClass.attackRange();
    }

    canFlyOverOtherCharacters() {
        return this.characterClass.canFlyOverOtherCharacters();
    }

    attack() {
        return this.characterClass.attack();
    }

    upgradeCharacterLevelAction() {
        return this.characterClass.upgradeCharacterLevelAction();
    }

    experienceForPlayer() {
        return this.characterClass.experienceForPlayer();
    }

    isInRoster() {
        if (this.player && this.player.roster) {
            return this.player.roster.activeCharacters().indexOf(this) >= 0;
        }
        return false;
    }

    isAlive() {
        return this.health > 0;
    }

    canJoinBattle() {
        return this.health >= this.maxHealth() / 3;
    }

    isTargetInRange() {
        return BattleHelper.isEnemyInRange(this, this.nextPlayTarget);
    }

    experienceRequiredToLevelUp() {
        return Math.round(this.experienceForLevel(this.getLevel()) - this.experience);
    }

    totalExperienceRequiredToLevelUp() {
        return Math.round(this.experienceForLevel(this.getLevel()) - this.experienceForLevel(this.getLevel()-1));
    }

    recoverAllHealth() {
        this.health = this.maxHealth();
    }

    /**
    * Reduce character's health with given damage
    * @param {number} damage
    */
    receiveDamage(damage) {
        this.health -= damage;
        if (this.health < 0) {
            this.health = 0;
        }
    }

    /**
    * Increases the character experience, potentially leveling up
    * @param {number} experience
    * @return (boolean) did level up?
    */
    earnExperience(experience) {
        let level = this.getLevel();
        this.experience += experience;
        return level != this.getLevel();
    }

    updateTime(deltaSeconds) {
        if (this.health < this.maxHealth()) {
            var heal = this.maxHealth() * deltaSeconds * this.healRate();
            this.health += heal;
            if (this.health > this.maxHealth()) {
                this.health = this.maxHealth();
            }
            return heal > 0;
        }
        return false;
    }

    /** EQUIPMENT **/
    getAllEquipment() {
        let equipped = [];
        for (let key in this.equipment) {
            equipped.push(this.equipment[key]);
        }
        return equipped;
    }
    getEquippedItemForType(type) {
        return this.equipment[type];
    }
    canEquip(item) {
        return this.player.getAllEquipment().indexOf(item) >= 0 &&
        !item.character &&
        !this.getEquippedItemForType(item.type) &&
        !(item.allowedClasses && item.allowedClasses.indexOf(this.characterClass.constructor.name) < 0) &&
        this.player.army.hero == this;
    }
    equip(item) {
        if (this.player.getAllEquipment().indexOf(item) < 0) {
            throw new ItemNotAvailableError();
        }
        if (item.character) {
            throw new ItemAlreadyEquippedError();
        }
        if (this.getEquippedItemForType(item.type)) {
            throw new HeroAlreadyEquippedError();
        }
        if (item.allowedClasses && item.allowedClasses.indexOf(this.characterClass.constructor.name) < 0) {
            throw new ItemNotCompatibleError();
        }
        if (this.player.army.hero != this) {
            throw new NonHeroCannotEquipError();
        }
        this.equipment[item.type] = item;
        item.character = this;
    }
    unequip(item) {
        if (item.character != this) {
            throw new ItemNotAvailableError();
        }
        this.equipment[item.type] = undefined;
        item.character = null;
    }

    unequipAll() {
        let char = this;
        this.getAllEquipment().forEach(function(item) {
            char.unequip(item);
        });
    }

    /// Debug representation (for logs)
    toString() {
        var loc = this.coordinate?this.coordinate:(this.initialCoordinate?this.initialCoordinate:"Out of Roster");
        return "{0}'s {1} (Level {2}) {3} ({4}/{5})".format(
            this.player.name,
            this.getName(),
            this.getLevel(),
            loc,
            Math.round(this.health),
            this.maxHealth()
        );
    }
    experienceForLevel(level) {
        return 100 * (level * (1 + level/10.0));
    }
    getLevel() {
        let i = 0;
        while (++i < 100) {
            if (this.experience < this.experienceForLevel(i)) {
                return i;
            }
        }
        return i;
    }
    actions() {
        return [
            new HealCharacterAction(this),
            this.upgradeCharacterLevelAction(),
        ].concat(this.characterClass.upgradeCharacterClassActions());
    }

}

class BaseCharacterClass {
    constructor(character) {
        this.character = character;
    }

    /** PROPERTIES THAT CAN BE OVERWRITEN BY SUBCLASSES **/

    /**
    * How far can characters of this class attack from. Min 1.
    * Could depend on level!
    * @returns {number}
    */
    attackRange() {
        return 1
    }

    /**
    * How many tiles can they move per each turn. Min 0 (No moves)
    * Could depend on level!
    * @returns {number}
    */
    moveRange() {
        return 4;
    }

    /**
    * Used to determine which characters move first. Higher means plays first.
    * Could depend on level!
    * @returns {number}
    */
    speed() {
        return 1
    }

    /**
    * How much damage can this character sustain before diing.
    * Could depend on level!
    * @returns {number}
    */
    maxHealth() {
        return Math.round(90 + this.character.getLevel() * 10);
    }

    /**
    * What % of maxHealth is recovered on each second.
    * Could depend on level!
    * @returns {number}
    */
    healRate() {
        return 0.01;
    }

    /**
    * Determines how much damage should an attack deliver. It could differ depending on the type of attacked enemy.
    * Could depend on level!
    * @param enemy {BaseCharacter}
    * @returns {number}
    */
    calculateAttackDamage() {
        let center = 8 + this.character.getLevel() * 2;
        let random = 4 + this.character.getLevel();
        return randomWithCenterAndSpread(center, random);
    }

    /**
    * Performs an attack action, which should affect at least ONE enemy (if in range).
    *
    * @returns {Array.<AttackResult>} Array of total attack results. In general is 1, see HeroesMage example for N.
    */
    attack() {
        if (!this.character.isTargetInRange()) {
            // If no enemies in range, try looking for a new one.
            BattleHelper.chooseWeakestEnemyInRangeFromCurrentLocation(this.character);
        }
        if (this.character.isTargetInRange()) {
            var damage = this.calculateAttackDamage();
            this.character.nextPlayTarget.receiveDamage(damage);
            return [new AttackResult(this.character.nextPlayTarget, damage)];
        }
        return [];
    }

    experienceForPlayer(player) {
        return 25 * this.character.getLevel();
    }

    experienceForLevel(level) {
        return 100 * (level * (1 + level/10.0));
    }

    costToMoveOnTerrain(terrain) {
        switch (terrain) {
            case kTerrainWater:
            return 1000;
            case kTerrainMountain:
            return 3;
            default:
            return 1;
        }
    }

    canLandOnTerrain(terrain) {
        return terrain != kTerrainWater;
    }

    canFlyOverOtherCharacters() {
        return false;
    }

    upgradeCharacterLevelAction() {
        return new UpgradeCharacterLevelAction(this.character, [
            Resource.human(1),
            Resource.gold(10)
        ]);
    }

    upgradeCharacterClassActions() {
        return [];
    }
}
