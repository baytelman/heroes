
class Army {
    constructor(player, uniqueName) {
        this.uniqueName = uniqueName;
        this.player = player;
        this.characters = [];
        this.hero = null;
    }
    setHero(character) {
        this.hero = character;
        character.initialCoordinate = new HexCoordinate(0, 0);
        this.addCharacter(character);
    }
    addCharacter(character) {
        if (character.army && character.army != this) {
            throw Error("Character belongs to another army");
        }
        character.army = this;
        character.player = this.player;

        if (!character.uniqueName) {
            character.uniqueName = "{0}_{1}_{2}".format(character.player.name, character.getShortName(), this.characters.length);
        }

        if (this.characters.indexOf(character) < 0) {
            this.characters.push(character);
        }
    }
    damagedCharacters() {
        return this.characters.filter(x => x.health < x.maxHealth());
    }
}
