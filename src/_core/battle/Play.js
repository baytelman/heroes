'use strict'

/**
 * Play component representing the movement before the attack
 */
class MovementPath {
    constructor(steps=[], costs=[]) {
        this.steps = steps;
        this.costs = costs;
        if (this.steps.length != this.costs.length) {
            throw new Error("wrong cost");
        }
    }

    maxCost() {
        if (this.costs.length < 1) {
            return 0;
        }
        return this.costs[this.costs.length - 1];
    }

    stepsCount() {
        if (!this.steps) {
            return 0;
        }
        return this.steps.length;
    }

    finalCoordinate() {
        return this.steps[this.steps.length - 1];
    }

    capAt(moveRange) {
        if (this.maxCost() <= moveRange) {
            return this;
        }
        for (var i = this.costs.length - 2; i >= 0; i--) {
            if (this.costs[i] <= moveRange) {
                return new MovementPath(this.steps.slice(0, i+1), this.costs.slice(0, i+1));
            }
        }
        return new MovementPath([], []);
    }
}

/**
 * Play component representing the attach after moving
 */
class AttackResult {
    constructor(target, damage) {
        this.target = target;
        this.damage = damage;
    }
}

/**
 * Represents a play (including a movement path and a attack
 * In the future, AttackResult could be ActionResult
 */
class Play {
    constructor(character, path, attackResults) {
        this.character = character;
        this.path = path;
        this.attackResults = attackResults;
    }
}
