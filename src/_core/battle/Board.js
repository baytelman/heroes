
class BoardDescriptor {
    constructor(width, height, defaultTerrain, terrainTiles) {
        this.width = width;
        this.height = height;
        this.defaultTerrain = defaultTerrain;
        this.terrainTiles = terrainTiles;
    }
    interpolateTerrain() {
        let originalTiles = this.terrainTiles;
        let newTiles = [];
        for (let x = 0; x < this.width; x++) {
            for (let y = 0; y < this.height; y++) {
                let coordinate = new HexCoordinate(x, y);
                let closest = TerrainTile.closestTerrainTileInArrayToCoordinate(originalTiles, coordinate);
                if (!closest) {
                    newTiles.push(new TerrainTile(this.defaultTerrain, coordinate));
                } else {
                    newTiles.push(new TerrainTile(closest.terrain, coordinate));
                }
            }
        }
        this.terrainTiles = newTiles;
    }
    static boardDescriptorWithURL(jsonUrl) {
        let json = loadTextFileSync(jsonUrl);
        let bd = CyclicalJSON.cyclicalObjectWithJSON(json);
        bd.interpolateTerrain();
        return bd;
    }
}

/**
* Represent a board, its players and alliances (teams of players), and their characters .
*/
class Board {
    constructor(nodeOrDescriptor) {
        if (nodeOrDescriptor instanceof MapNode) {
            var node = nodeOrDescriptor;
            this.node = node;
            this.descriptor = node.getBoardDescriptor();
        } else if (nodeOrDescriptor instanceof BoardDescriptor) {
            var boardDescriptor = nodeOrDescriptor;
            this.descriptor = boardDescriptor;
        } else {
            throw new Error("missing descriptor");
        }
        this.width = this.descriptor.width;
        this.height = this.descriptor.height;
        this.terrainTiles = this.descriptor.terrainTiles;

        this.alliances = new Map();
        this.winners = false;
        this.characters = [];
    }

    addPlayer(player, alliance) {
        player.alliance = alliance;
        if (!this.alliances[alliance]) {
            this.alliances[alliance] = [];
        }
        this.alliances[alliance].push(player);
    }

    /**
    * Adds a character to the board at a specific coordinate, under control of given player.
    * The character begins with full health.
    * @param {Player} player
    * @param {BaseCharacter} character
    * @param {number} x
    * @param {number} y
    */
    addCharacter(player, character, coordinate) {
        character.board = this;
        character.player = player;
        this.characters.push(character);

        character.turn = 0;

        if (character.initialCoordinate === undefined && coordinate === undefined) {
            throw Error("Neither initial coordinate or custom coordinate provided");
        }

        if (coordinate) {
            character.coordinate = coordinate;
        } else if (character.initialCoordinate) {
            character.coordinate = character.initialCoordinate;
        }

        if (!character.uniqueName) {
            character.uniqueName = "{0}_{1}_{2}".format(character.player.name, character.getShortName(), this.characters.length);
        }
    }
    addEnemyPlayerWithCharactersFromRosterDescriptor(rosterDescriptor, baseExperience = 0) {
        var player = new Player("Enemy", 99);
        rosterDescriptor.characters.forEach(function(character) {
            character.earnExperience(baseExperience);
            character.recoverAllHealth();
        });
        var r = new Roster(player);
        r.characters = rosterDescriptor.characters;
        this.addPlayerAndCharactersFromRoster(r, new HexCoordinate(this.width - 7, Math.round(this.height/2)));
    }

    addPlayerAndCharactersFromRoster(roster, center = new HexCoordinate(0,0)) {
        var board = this;
        this.addPlayer(roster.player, roster.player.alliance);
        roster.activeCharacters().forEach(function(character) {
            if (character.canJoinBattle()) {
                var coord = new HexCoordinate(character.initialCoordinate.x + center.x, character.initialCoordinate.y + center.y);
                board.addCharacter(roster.player, character, coord);
            }
        });
    }

    winnerAlliance() {
        if (this.winners) {
            return this.winners;
        }
        var aliveAlliances = [];
        this.characters.forEach(function(character) {
            if (character.isAlive() && aliveAlliances.indexOf(character.player.alliance) < 0) {
                aliveAlliances.push(character.player.alliance);
            }
        });
        if (aliveAlliances.length == 1) {
            return this.winners = this.alliances[aliveAlliances[0]];
        }
        return null;
    }

    isOver() {
        return !! this.winners;
    }

    /**
    * Returns true if there is no alive characters in that coordinate.
    * @param {HexCoordinate} c
    * @returns {boolean}
    */
    isCoordinateAvailable(c) {
        if (c.x < 0 || c.x >= this.width || c.y < 0 || c.y >= this.height) {
            return false;
        }
        var taken = false;
        this.characters.forEach(function (character) {
            if (taken || (character.isAlive() && character.coordinate.isEqualTo(c))) {
                taken = true;
            }
        });
        return !taken;
    }

    terrainAtCoordinate(c) {
        if (this.terrainTiles) {
            for (var i = 0; i < this.terrainTiles.length; i++) {
                if (this.terrainTiles[i].coordinate.isEqualTo(c)) {
                    return this.terrainTiles[i].terrain;
                }
            }
        }
        return kTerrainGround;
    }

    /**
    * Return all alive characters who are not in the same alliance than the given character.
    * @param {BaseCharacter} character
    * @returns {Array.<BaseCharacter>}
    */
    aliveEnemiesForCharacter(character) {
        var enemies = [];
        this.characters.forEach(function (anotherChar) {
            if (anotherChar.player.alliance != character.player.alliance && anotherChar.isAlive()) {
                enemies.push(anotherChar);
            }
        });
        return enemies;
    }

    /**
    * Which characters are next to play.
    * @returns {Array.<BaseCharacter>} Ordered list of which characters will play next
    */
    nextCharactersToPlay() {
        var nextToPlay = this.characters.slice();
        nextToPlay.sort(function (a, b) {
            if (!a.isAlive()) {
                return 1;
            }
            if (!b.isAlive()) {
                return -1;
            }
            return (a.turn - b.turn) * 1000000 + (Effect.modifiedSpeedForCharacter(b) - Effect.modifiedSpeedForCharacter(a));
        });
        return nextToPlay;
    }

    allianceDidWin(players, rewards) {
        return;
    }

    /**
    * Calculates and executes a play for the next character
    */
    nextCharacterMovesAndAttacks() {
        var character = this.nextCharactersToPlay()[0];
        character.chooseTargetAmongAliveEnemies();
        var destination = BattleHelper.characterMoves(character);
        var attackResults = character.attack();
        character.turn++;

        var attacked = "";

        if (attackResults == null || attackResults.length < 1) {
            attacked = "did not attack"
        } else {
            attackResults.forEach(function (attackResult) {
                if (attacked.length > 0) {
                    attacked += ", ";
                }
                attacked += "attacked {0} inflicting {1} damage".format(attackResult.target, attackResult.damage);
            });
            console.log("{0} {2}.".format(character, attacked));
        }

        attackResults.forEach(function (attackResult) {
            if (!attackResult.target.isAlive()) {
                console.log("- {0} dies.".format(attackResult.target));
            }
        });
        var winners = this.winnerAlliance();
        if (this.isOver() && this.allianceDidWin) {
            var board = this;
            if (winners) {
                var rewards = [];
                var levelUps = [];
                if (board.node) {
                    winners.forEach(function(player) {
                        if (board.node.map && board.node.map.player) {
                            if (board.node.map.player == player) {
                                let charsToBeRewardedXP = board.characters.filter(x => player.roster.activeCharacters().indexOf(x) >= 0);
                                board.node.rewardRoster(player, charsToBeRewardedXP).forEach(function(levelUp) {
                                    levelUps.push(levelUp);
                                });
                                let r = board.node.resourceRewardsWithTime();
                                player.earnResources(r);
                                r.forEach(function(reward) {
                                    rewards.push(reward);
                                });
                                board.node.map.player.trackRewardForMapNodeWithId(board.node.map.id, board.node.id, kRewardResources);

                                let wait = board.node.waitUntilCanRewardPlayer(player);
                                if (wait !== null && wait <= 0) {
                                    board.node.rewardPlayer(player).forEach(function(reward) {
                                        rewards.push(reward);
                                    });
                                    board.node.map.player.trackRewardForMapNodeWithId(board.node.map.id, board.node.id, kRewardReward);
                                } else {
                                    console.log("Not ready to reward player with characters yet.");
                                }
                                board.node.map.unlock(board.node.id);
                                board.node.map.player.unlockedMapNodeWithId(board.node.map.id, board.node.id);
                            }
                        }
                    });
                }
                board.allianceDidWin(winners, rewards, levelUps);
            }
        }
        return new Play(character, destination, attackResults);
    }
}
