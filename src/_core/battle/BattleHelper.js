
class BattleHelper {
    /**
    * Moves the character along it's next play's path and returns the performed steps
    */
    static characterMoves(character) {
        var cappedMovement = new MovementPath();
        var final = character.coordinate;

        if (character.nextPlayPath && character.nextPlayPath.steps.length > 0) {
            cappedMovement = character.nextPlayPath.capAt(Effect.modifiedMoveRangeForCharacter(character));
            var steps = cappedMovement.steps;
            if (steps.length > 0) {
                final = steps[steps.length - 1];
            }
        }
        character.coordinate = final;

        return cappedMovement;
    }

    /**
    * Returns if an enemy is in range from the current coordinate (usually after moving)
    * @param {BaseCharacter} enemy
    * @returns {boolean}
    */
    static isEnemyInRange(character, enemy) {
        if (enemy && character.coordinate && enemy.coordinate && character.coordinate.distanceTo(enemy.coordinate) <= Effect.modifiedAttackRangeForCharacter(character)) {
            return true;
        }
        return false;
    }

    static characterNextPlayStepsCost(character) {
        if (!character.nextPlayPath) {
            return 0;
        }
        return character.nextPlayPath.maxCost();
    }

    /**
    * Returns if path can be completed in one turn
    * @param path
    * @returns {boolean}
    */
    static isPathInCharacterMoveRange(character, path) {
        return path.maxCost() <= Effect.modifiedMoveRangeForCharacter(character);
    }

    static setNextPlay(character, enemy, destination) {
        character.nextPlayTarget = enemy;
        character.nextPlayPath = destination;
    }

    /**
    * Given a list of enemies, it will find the weakest of those that are in range.
    * If no enemy was in range, it will find the weakest among those out of range.
    *
    * This function is combined, because it's expensive to check
    * if character is in range or not, so we do both at once.
    *
    * @param {Array.<BaseCharacter>} enemiesArray
    */
    static chooseWeakestEnemyPrioritizingWithinRange(character) {
        let moveRange = Effect.modifiedMoveRangeForCharacter(character);
        var enemiesArray = character.board.aliveEnemiesForCharacter(character);
        var weakestInRange = null;
        var weakestInRangeDestination = null;
        var weakestOutOfRange = null;
        var weakestOutOfRangeDestination = null;
        enemiesArray.forEach(function (enemy) {
            // Already found one in range weaker than Enemy
            if (weakestInRange && weakestInRange.health < enemy.health) {
                return;
            }
            // Already found one closer weaker than Enemy
            if (weakestOutOfRange &&
                weakestOutOfRange.coordinate.distanceTo(character.coordinate) < enemy.coordinate.distanceTo(character.coordinate)) {
                    return;
                }
                var path = BattleHelper.bestPathForCharacterToAttackTarget(character, enemy);
                if (path) {
                    if (BattleHelper.isPathInCharacterMoveRange(character, path)) {
                        if (!weakestInRange || enemy.health < weakestInRange.health) {
                            weakestInRange = enemy;
                            weakestInRangeDestination = path;
                        }
                    } else {
                        if (!weakestOutOfRange || enemy.health < weakestOutOfRange.health ||
                            (enemy.health == weakestOutOfRange.health
                                && path.maxCost() < BattleHelper.characterNextPlayStepsCost(weakestOutOfRangeDestination))) {

                                    weakestOutOfRange = enemy;
                                    weakestOutOfRangeDestination = path;
                                }
                            }
                        }
                    });
                    if (weakestInRange) {
                        BattleHelper.setNextPlay(character, weakestInRange, weakestInRangeDestination);
                    } else {
                        BattleHelper.setNextPlay(character, weakestOutOfRange, weakestOutOfRangeDestination);
                    }
                }

                /**
                * Given a list of enemies, it will find the weakest of those that are in range.
                * If no enemy was in range, it will find the weakest among those out of range.
                *
                * This function is combined, because it's expensive to check
                * if character is in range or not, so we do both at once.
                *
                * @param {Array.<BaseCharacter>} enemiesArray
                */
                static chooseWeakestEnemyInRangeFromCurrentLocation(character) {
                    var weakestInRange = null;
                    var weakestInRangeDestination = null;
                    var enemiesArray = character.board.aliveEnemiesForCharacter(character);
                    enemiesArray.forEach(function (enemy) {
                        // Already found one in range weaker than Enemy
                        if (weakestInRange && weakestInRange.health < enemy.health) {
                            return;
                        }
                        if (character.coordinate.distanceTo(enemy.coordinate) <= Effect.modifiedAttackRangeForCharacter(character)) {
                            weakestInRange = enemy;
                            weakestInRangeDestination = new MovementPath();
                        }
                    });
                    if (weakestInRange) {
                        BattleHelper.setNextPlay(character, weakestInRange, null);
                    }
                }


                /**
                * Calculates the shortest path for character to get in range to attack enemy
                * It could eventually choose the SAFEST too (farthest from other enemies)
                * @param {BaseCharacter} character - The one attacking
                * @param {BaseCharacter} enemy - The one being attacked
                * @returns {MovementPath}
                */
                static bestPathForCharacterToAttackTarget(character, enemy) {
                    if (BattleHelper.isEnemyInRange(character, enemy)) {
                        return new MovementPath();
                    }

                    let moveRange = Effect.modifiedMoveRangeForCharacter(character);
                    var limit = Math.max(
                        character.board.height + character.board.width / 2,
                        character.board.width + character.board.height / 2);
                        var closestDestination = null;
                        var minStepsPerCoordinate = {};

                        /* Let's find the best path! And I mean ALL OF THEM */
                        /* pathsFinder returns true if the current move is within move range */
                        var pathsFinder = function (startCoordinate, previousSteps=[], previousStepsCosts=[], maxReversePoints=4) {
                            let t = character.board.terrainAtCoordinate(startCoordinate);

                            let previousStepInRange = true;
                            let totalCost = 0;
                            /* If this is the initial coordinate, cost is zero. If not, cost is aggregated cost + this cost */
                            if (previousStepsCosts.length > 0) {
                                previousStepInRange = previousStepsCosts[previousStepsCosts.length-1] <= moveRange;
                                totalCost = previousStepsCosts[previousStepsCosts.length-1] + character.costToMoveOnTerrain(t);
                            }

                            /* If
                            - My previous step was the LAST one in range
                            - and now I'm out of move range (meaning I need to land in the previous one)
                            - BUT I cannot land in THAT one:
                            Then this path is INVALID */
                            if (previousStepInRange && totalCost > moveRange) {
                                let lastC = previousSteps[previousSteps.length-1];
                                let lastT = character.board.terrainAtCoordinate(lastC);
                                if (!character.canLandOnTerrain(lastT) || !character.board.isCoordinateAvailable(lastC)) {
                                    return;
                                }
                            }

                            /* If I already got to the destination with this or less cost, then skip this path */
                            if (closestDestination && closestDestination.maxCost() <= totalCost) {
                                return;
                            }

                            previousStepsCosts.push(totalCost);
                            previousSteps.push(startCoordinate);

                            // Ensure we don't visit the same path again through a longer set of steps:
                            var alreadyHere = minStepsPerCoordinate[startCoordinate.toString()];
                            if (alreadyHere && alreadyHere <= totalCost) {
                                return;
                            }
                            minStepsPerCoordinate[startCoordinate.toString()] = totalCost;

                            if (startCoordinate.distanceTo(enemy.coordinate) <= Effect.modifiedAttackRangeForCharacter(character)
                            && character.canLandOnTerrain(t)
                            && character.board.isCoordinateAvailable(startCoordinate)) {
                                closestDestination = new MovementPath(previousSteps, previousStepsCosts);
                                // If I'm on range, my recursions won't get any closer: return!
                                return;
                            }

                            if (previousSteps.length >= limit) {
                                return;
                            }

                            var hoods = startCoordinate.neighborCoordinates();
                            for (var i in hoods) {
                                var coord = hoods[i];
                                if (coord.isInArray(previousSteps)) {
                                    continue;
                                }

                                if (!character.canFlyOverOtherCharacters() && !character.board.isCoordinateAvailable(coord)) {
                                    continue;
                                }

                                var mMax = maxReversePoints;
                                if (Math.abs(coord.x - enemy.coordinate.x) > Math.abs(startCoordinate.x - enemy.coordinate.x)) {
                                    mMax--;
                                }
                                if (Math.abs(coord.y - enemy.coordinate.y) > Math.abs(startCoordinate.y - enemy.coordinate.y)) {
                                    mMax--;
                                }

                                if (mMax <= 0) {
                                    continue;
                                }

                                var mySteps = previousSteps.slice();
                                var myStepsCosts = previousStepsCosts.slice();
                                pathsFinder(coord, mySteps, myStepsCosts, mMax);
                            }
                        };
                        pathsFinder(character.coordinate);

                        if (closestDestination) {
                            //Let's remove the first step (because it's the start)
                            closestDestination.costs.shift();
                            closestDestination.steps.shift();
                        }
                        return closestDestination;
                    }
                }
