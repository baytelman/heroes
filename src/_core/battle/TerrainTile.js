
const kTerrainGround = 'ground';
const kTerrainMountain = 'mountain';
const kTerrainWater = 'water';

class TerrainTile {
    constructor(terrain, coordinate, radius) {
        this.terrain = terrain;
        this.coordinate = coordinate;
        this.radius = radius;
    }
    static closestTerrainTileInArrayToCoordinate(tileArray, coordinate) {
        let minD = null;
        let closest = null;
        tileArray.forEach(function(tile) {
            let d = tile.coordinate.distanceTo(coordinate);
            if (tile.radius !== undefined && tile.radius < d) {
                return;
            }
            if (minD === null || minD > d) {
                minD = d;
                closest = tile;
            }
        });
        return closest;
    }
}
