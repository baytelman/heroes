
class RosterError extends Error {
}

class Roster {
    constructor(player) {
        this.player = player;
        this.characters = [];
    }
    static rosterWithURL(jsonUrl) {
        let json = loadTextFileSync(jsonUrl);
        let roster = CyclicalJSON.cyclicalObjectWithJSON(json);
        roster.characters.forEach(function(character) {
            if (typeof character.characterClass === 'string') {
                character.setClass(eval(character.characterClass));
            }
        });
        return roster;
    }
    addCharacter(character, coordinate) {
        if (character.army != this.player.army) {
            throw new RosterError("Character belongs to another army");
        }
        if (!coordinate) {
            coordinate = character.initialCoordinate;
        }
        if (!coordinate) {
            throw new RosterError("Character needs initial coordinate");
        }
        this.activeCharacters().forEach(function(anotherCharacter) {
            if (anotherCharacter == character) {
                throw new RosterError("Character already is in the roster");
            }
        });
        character.initialCoordinate = coordinate;
        this.characters.push(character);
    }
    availableCoordinates() {
        var center = new HexCoordinate(0,0);
        var coordinates = [center];
        center.neighborCoordinates(2).forEach(function(coord) {
            coordinates.push(coord);
        });
        let count = Math.round(Math.pow(this.player.army.hero.getLevel(), 0.6));
        coordinates = coordinates.slice(0, count);
        return coordinates;
    }
    activeCharacters() {
        var r = this.characters.slice();
        if (this.player.army && this.player.army.hero) {
            r.push(this.player.army.hero);
        }
        return r;
    }
    removeCharacterWithUniqueName(uniqueName) {
        this.characters = this.characters.filter(function(character) {
            if (character.uniqueName == uniqueName) {
                character.initialCoordinate = null;
                return false;
            }
            return true;
        });
    }
    addCharacterWithUniqueName(uniqueName, coordinate) {
        var roster = this;
        this.player.army.characters.forEach(function(character) {
            if (character.uniqueName == uniqueName) {
                roster.addCharacter(character, coordinate);
            }
        });
    }
    checkConsistency() {
        var availableCoordinates = this.availableCoordinates();
        var roster = this;
        roster.activeCharacters().forEach(function(character) {
            roster.activeCharacters().forEach(function(anotherCharacter) {
                if (anotherCharacter != character) {
                    if (anotherCharacter.initialCoordinate.isEqualTo(character.initialCoordinate)) {
                        throw new RosterError("Character overlaps another character");
                    }
                }
            });
            if (!character.initialCoordinate.isInArray(availableCoordinates)) {
                throw new RosterError("Character in unavailable coordinate");
            }
        });
    }
}
