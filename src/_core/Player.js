/**
* Created by fbaytelm on 8/11/15.
*/

var HeroesVersion = "v0.1";

class Player {
    constructor(name, alliance=-1) {
        this.name = name;
        this.alliance = alliance;

        this.roster = new Roster(this);
        this.army = new Army(this);

        this.time = 0;

        this.unlockedNodes = {};
        this.rewards = {};
        this.resources = {};
        this.equipment = [];
    }
    asJSON() {
        let classesToEncode = {
            "Player":null,
            "Army":["Player"],
            "BaseItem":["Character"],
            "Character":["BaseItem", "Army", "Character", "Player"],
            "BaseCharacterClass":["Character"],
            "SquareCoordinate":null
        };
        return CyclicalJSON.cyclicalObjectAsJSON(this, classesToEncode);
    }
    static playerWithJSON(json) {
        return CyclicalJSON.cyclicalObjectWithJSON(json, function(player) {
            player.army.characters.forEach(function(character) {
                if (player.army.hero != character && character.initialCoordinate) {
                    player.roster.addCharacter(character);
                }
            });
            player.roster.checkConsistency();
        });
        return player;
    }
    updateTime(deltaSeconds) {
        var updated = [];
        this.time += deltaSeconds;

        /* Update characters */
        this.army.characters.forEach(function(character) {
            if (character.updateTime(deltaSeconds)) {
                updated.push(character);
            }
        });
        if (updated.length > 0 && this.charactersDidUpdate) {
            this.charactersDidUpdate(updated);
        }
    }
    trackRewardForMapNodeWithId(mapId, nodeId, rewardId) {
        if (! this.rewards[mapId]) {
            this.rewards[mapId] = {};
        }
        if (! this.rewards[mapId][nodeId + rewardId]) {
            this.rewards[mapId][nodeId + rewardId] = [];
        }
        this.rewards[mapId][nodeId + rewardId].push(this.time);
    }
    unlockedMapNodeWithId(mapId, nodeId) {
        if (! this.unlockedNodes[mapId]) {
            this.unlockedNodes[mapId] = {};
        }
        if (! this.unlockedNodes[mapId][nodeId]) {
            this.unlockedNodes[mapId][nodeId] = [];
        }
        this.unlockedNodes[mapId][nodeId].push(this.time);
    }
    unlockedNodeIdsForMapWithId(mapId) {
        if (! this.unlockedNodes[mapId]) {
            return {};
        }
        return this.unlockedNodes[mapId];
    }
    unlockedNodesFromMapDictionary(dictionary) {
        var nodes = [];
        for (var key in this.unlockedNodes) {
            var map = dictionary[key];
            var unlockedIds = this.unlockedNodes[key];
            for (var id in unlockedIds) {
                let node = map.nodeWithId(id);
                nodes.push(node);
            }
        }
        return nodes;
    }
    timeElapsedSinceLastRewardForMapNodeWithId(mapId, nodeId, rewardId) {
        let p = this.rewardCoundForMapNodeWithId(mapId, nodeId, rewardId);
        if (p) {
            let uTimes = this.rewards[mapId][nodeId+rewardId];
            return this.time - uTimes[p - 1];
        }
        return null;
    }
    rewardCoundForMapNodeWithId(mapId, nodeId, rewardId) {
        if (! this.rewards[mapId]) {
            return null;
        }
        if (! this.rewards[mapId][nodeId+rewardId]) {
            return null;
        }
        let uTimes = this.rewards[mapId][nodeId+rewardId];
        return uTimes.length;
    }
    /* Resources */
    earnResources(resources) {
        let player = this;
        resources.forEach(function(r) {
            player.earnResource(r);
        });
    }
    earnResource(resource) {
        if (!this.resources[resource.type]) {
            this.resources[resource.type] = 0;
        }
        this.resources[resource.type] += resource.amount;
    }
    spendResource(resource) {
        if (resource.amount > 0) {
            if (!this.resources[resource.type] || this.resources[resource.type] < resource.amount) {
                throw new InsuficientResourcesError();
            }
            this.resources[resource.type] -= resource.amount;
        }
    }
    getResourceAmountForType(type) {
        if (!this.resources[type]) {
            return this.resources[type] = 0;
        }
        return this.resources[type];
    }
    /* Equipment */
    earnItem(item) {
        this.equipment.push(item);
    }
    getAllEquipment() {
        return this.equipment;
    }
}
