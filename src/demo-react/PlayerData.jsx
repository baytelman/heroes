
var PlayerDataComponent = React.createClass({
    timer:null,
    getInitialState: function() {
        return {name: this.props.data.name, gold: this.props.data.getResourceAmountForType(kResourceGold) };
    },
    componentDidMount: function () {
        var self = this;
        self.timer = setInterval(function () {
            var state = {};
            kResourceTypes.forEach(function(type) {
                state[type] = self.props.data.getResourceAmountForType(type).toFixed(1);
            });
            self.setState(state);
        }, 500);
    },
    componentWillUnmount: function () {
        clearInterval(this.timer);
    },
    render: function() {
        var resources = kResourceTypes.map(function(type) {
            var className = "resource_" + type;
            return (<resource className={className}>
                <abbr title={type}>
                    {type[0]}{this.state[type]}
                </abbr>
            </resource>);
        }.bind(this));
        return (
            <div id="player-data">
                <resources>
                    { resources }
                </resources>
            </div>
        );
    }
});
