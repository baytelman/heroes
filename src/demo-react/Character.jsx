
var CharacterComponent = React.createClass({
    timer:null,
    getInitialState: function() {
        return {
            name: this.props.data.info.getName(),
            experience:{
                level: Math.floor(this.props.data.info.getLevel()),
                current: Math.floor(this.props.data.info.experience),
                next_level_experience: Math.floor(this.props.data.info.experienceRequiredToLevelUp() )
            },
            health:{
                max:this.props.data.info.maxHealth(),
                current: Math.floor(this.props.data.info.health)
            },
            selected:false,
        };
    },
    markSelected:function(selected) {
        this.setState({selected: selected?true:false});
    },
    componentDidMount: function () {
        this.timer = setInterval(function () {
            this.setState({
                name: this.props.data.info.getName(),
                experience:{
                    level: Math.floor(this.props.data.info.getLevel()),
                    current: Math.floor(this.props.data.info.experience),
                    next_level_experience: Math.floor(this.props.data.info.experienceRequiredToLevelUp() )
                },
                health:{
                    max:this.props.data.info.maxHealth(),
                    current: Math.floor(this.props.data.info.health)
                }
            });

        }.bind(this), 250);
    },
    componentWillUnmount: function () {
        clearInterval(this.timer);
    },
    updateSelected: function(e){
        console.log('updateSelected',this.props);
    },
    isActionAffordable: function(action) {
        return action.isAffordable(this.props.data.info.player);
    },
    actionCosts: function(action) {
        var actions = this.props.data.info.actions();
        if (actions[action]) {
            return actions[action].costs();
        }
        return [];
    },
    onAction: function(action) {
        action.executeForPlayer(this.props.data.info.player);
        this.forceUpdate();
    },
    render: function() {
        var className = this.props.data.info.canJoinBattle()?"":" disabled";
        if (this.props.data.isActive) {
            className += " out-of-roster";
        } else {
            className += " active";
        }

        if( this.state.selected)
        className += " selected";

        var icon = "";
        try {
            icon = emojione.shortnameToImage(getIcon(this.props.data.info.characterClass.constructor.name));
            icon = icon.replace('src="//','src="http://');
        } catch (e) {
        }

        var id = "ract_roster_" + this.props.data.info.uniqueName;
        function setIcon() { return {__html: icon }; };

        var stateStyle = {
            width: Math.floor( (this.state.health.current/this.state.health.max)*100 ) + "%"
        };

        var actions = this.props.data.info.actions();
        var available = [];
        var disabled = [];
        actions.map(function(action) {
            if (action.isAvailable()) {
                if (this.isActionAffordable(action)) {
                    available.push(action);
                } else {
                    disabled.push(action);
                }
            }
        }.bind(this));
        var actionLinks = available.concat(disabled).map(function(action) {
            var costs = action.costs();
            var link;
            if (this.isActionAffordable(action)) {
                link = (<a href="javascript:" onClick={this.onAction.bind(this, action)}>{ action.code() }</a>);
            } else {
                link = (<span>({ action.code() })</span>);
            }
            return (<abbr title={costs}>{ link }</abbr>);
        }.bind(this));

        return (
            <character className={className} onClick={this.props.onClick}  id={id} style={this.props.data.position}>
                <name>{this.state.name}</name>
                <actions>
                    { actionLinks }
                </actions>
                <experience>
                    <next>{this.state.experience.next_level_experience}</next>
                </experience>
                <health>
                    <bar><state style={stateStyle} /></bar>
                    <current>{this.state.health.current}</current>
                    <max>{this.state.health.max}</max>
                </health>
                <icon dangerouslySetInnerHTML={setIcon()} ></icon>
            </character>
        );
    }
});
