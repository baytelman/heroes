var TileComponent = React.createClass({
  getInitialState: function() {
      return {available:  this.props.data.available};
  },
  render: function() {
    //console.log("render-tile",this.props.data.available);
    var className = "";
    if(this.props.data.available)
      className = "free";
    return (
      <tile className={className} onClick={this.props.onClick} style={this.props.data.position}></tile>
    );
  }
});