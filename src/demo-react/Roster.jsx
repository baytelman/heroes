var RosterComponent = React.createClass({
    timer:null,
    __listAvailable:false,
    showAvailable:function(enable){
        this.__listAvailable = enable;
        this.setState({showAvailable: this.__listAvailable });
    },
    getInitialState: function() {
        return {
            showAvailable: this.__listAvailable,
            availableCharacters: 0,
            availableCoordinates: 0
        };
    },
    componentDidMount: function () {
        var self = this

        self.timer = setInterval(function () {

            self.setState({
                availableCharacters: self.props.data.roster.activeCharacters().length,
                availableCoordinates: self.props.data.roster.availableCoordinates().length
            });

        }, 250);

    },
    componentWillUnmount: function () {
        clearInterval(this.timer);
    },
    removeCharacterFromRoster:function(uniqueName){
        this.props.data.roster.removeCharacterWithUniqueName(uniqueName);
        this.setState({availableCharacters: this.props.data.roster.activeCharacters().length});
    },
    tileSelect:function(coordinate)
    {
        console.log(arguments);
        this.props.onTileSelect(coordinate);
    },
    topLeft : function(x, y) {
        var top = Math.round(y * Game.options.roster.tile.height);
        var left = Math.round((x + ((y+1) % 2)/2.0) * Game.options.roster.tile.width);
        return [top, left];
    },
    render: function() {
        var theObj = this;
        var minX = 0;
        var minY = 0;
        var maxY = 0;
        this.props.data.roster.availableCoordinates().forEach(function (coordinate) {
            if (coordinate.x < minX)
            minX = coordinate.x;
            if (coordinate.y < minY)
            minY = coordinate.y;
            if (coordinate.y > maxY)
            maxY = coordinate.y;
        });

        minY = Math.floor(minY / 2) * 2;

        var tiles = this.props.data.roster.availableCoordinates().map(function (coordinate) {
            var top_left = rosterTopLeft(coordinate.x - minX, coordinate.y - minY);
            var tiledata =  {
                position:{
                    top: top_left[0] + "px",
                    left:  top_left[1] + "px"
                },
                available: this.state.showAvailable
            };

            return (
                <TileComponent onClick={this.tileSelect.bind(this,coordinate)} data={tiledata} />
            );
        }.bind(this));
        var characters = this.props.data.roster.activeCharacters().map(function(character){
            var top_left = this.topLeft(character.initialCoordinate.x - minX, character.initialCoordinate.y - minY);
            var chardata =  {
                type: "roster",
                position:{
                    top: top_left[0] + "px",
                    left: top_left[1] + "px"
                },
                info: character
            };
            return (
                <CharacterComponent onClick={this.removeCharacterFromRoster.bind(this, character.uniqueName)} data={chardata} />
            );
        }.bind(this));
        var rosterStyle =	{
            height: ( (maxY - minY + 2) * rosterTileHeight )+ "px"
        }

        return (
            <roster style={rosterStyle}>
                {tiles}
                {characters}
            </roster>
        );
    }
});
