
var ArmyComponent = React.createClass({
    timer:null,
    selectedCharacter: null,
    getInitialState: function() {

        return {
            availableCharacters: 0,
            availableCoordinates: 0
        };
    },
    componentDidMount: function () {
        var self = this
        self.timer = setInterval(function () {
            self.setState({
                availableCharacters: self.props.data.roster.activeCharacters().length,
                availableCoordinates: self.props.data.roster.availableCoordinates().length
            });
        }, 250);

    },
    componentWillUnmount: function () {
        clearInterval(this.timer);
    },
    selectCharacter : function(character,event){
        this.selectedCharacter = character;

        self = this;
        this.props.data.army.characters.map(function(chr){
            var ref = "c" + self.props.data.army.characters.indexOf(chr);
            self.refs[ref].markSelected(false);
        });

        var ref = "c" + this.props.data.army.characters.indexOf(character);
        this.refs[ref].markSelected(true);

        this.props.onCharacterSelect(character);

    },
    unselectAll:function(){
        this.selectedCharacter = null;
        self = this;
        this.props.data.army.characters.map(function(chr){
            var ref = "c" + self.props.data.army.characters.indexOf(chr);
            self.refs[ref].markSelected(false);
        });
    },
    render : function() {
        var characters = this.props.data.army.characters.map(function(character){
            var chardata =  {
                type: "army",
                position:{  },
                info: character,
                isActive: this.props.data.roster.activeCharacters().indexOf(character) < 0
            };
            var ref = "c" + this.props.data.army.characters.indexOf(character);
            return (
                <CharacterComponent onClick={this.selectCharacter.bind(this, character)} data={chardata} ref={ref} />
            );
        }.bind(this));

        return (
            <army>{characters}</army>
        );

    }
});
