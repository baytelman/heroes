
var PlayerControlBar = React.createClass({
  currentCaracter:null,
  currentCoordinate: null,
  selectCharacter: function(character){
    var actives = this.props.data.roster.activeCharacters();
    if(actives.indexOf(character) < 0)
    {
      this.currentCaracter = character;
      this.addToRoster();
      this.refs.roster.showAvailable(true);
    }
    else{
      this.currentCaracter = null;
      this.refs.roster.showAvailable(false);
    }
  },
  selectTile: function(tile){
    if( this.currentCaracter ){
      this.currentCoordinate = tile;
      this.addToRoster();
    }
    else{
      this.currentCoordinate = null;
      this.refs.army.unselectAll();
    }
  },
  addToRoster:function(){
    if(this.currentCaracter && this.currentCoordinate)
    {
      try{
        this.props.data.roster.addCharacterWithUniqueName(this.currentCaracter.uniqueName, this.currentCoordinate);
      }catch(e){
        //Character already in the roster
      }
      this.refs.roster.showAvailable(false);
      this.refs.army.unselectAll();
      this.currentCaracter = this.currentCoordinate = null;
    }
  },
  render: function() {
    return (
     <div id="bar">
   	  <div id="roster-2">
      	<RosterComponent onTileSelect={this.selectTile} data={this.props.data} ref="roster" />
      </div>
      <div id="tools">
      	<PlayerDataComponent data={this.props.data} />
      	<div id="army-2">
      		<ArmyComponent onCharacterSelect={this.selectCharacter} data={this.props.data} ref="army" />
      	</div>
      </div>
     </div>
    );
  }
});
