var rosterTileWidth = 40;
var rosterTileHeight = 46.2;

function renderRoster(roster,showFreeSlots) {
    var minX = 0;
    var minY = 0;
    var maxY = 0;
    roster.availableCoordinates().forEach(function (coordinate) {
        if (coordinate.x < minX)
            minX = coordinate.x;
        if (coordinate.y < minY)
            minY = coordinate.y;
        if (coordinate.y > maxY)
            maxY = coordinate.y;
    });
    minY = Math.floor(minY / 2) * 2;

    var html = "<roster style='height:" + (maxY - minY + 2) * rosterTileHeight + "px;'>";
    var coordinates = roster.availableCoordinates();
    coordinates.forEach(function (coordinate) {
        var top_left = rosterTopLeft(coordinate.x - minX, coordinate.y - minY);
        var location = 'top:' + top_left[0] + 'px;left:' + top_left[1] + 'px;';
        var clss = showFreeSlots?"free":"";
        html += "<tile id='roster" + coordinate.toJString() +"' style='" + location + "' class='"+clss+"'></tile>";
    });

    roster.activeCharacters().forEach(function(character){
        var top_left = rosterTopLeft(character.initialCoordinate.x - minX, character.initialCoordinate.y - minY);
        var location = 'top:' + top_left[0] + 'px;left:' + top_left[1] + 'px;';
        var className = character.player.name + (character.canJoinBattle()?"":" disabled");
        var icon = "";
        try {
            icon = emojione.shortnameToImage(getIcon(character.characterClass.constructor.name));
            icon = icon.replace('src="//','src="http://');
        } catch (e) {
        }
        html += "<character id='roster_" + character.uniqueName + "' class='" + className + "' style='" + location + "'>"
        + " <icon>"+  icon +"</icon>"
        + "</character>";
    });


    html += "</roster>";
    $("#roster").html(html);

    roster.activeCharacters().forEach(function(character){
        $('#roster_' + character.uniqueName).click(function() {

            var $node = $(this);
            if($node.parents(".disabled").length > 0) return;

            roster.removeCharacterWithUniqueName(character.uniqueName);
            renderRoster(roster);
            renderArmy(roster.player.army);
        });
    });

    coordinates.forEach(function (coordinate) {
        $('#roster' + coordinate.toJString()).click(function() {
            roster.addCharacterWithUniqueName(armySelected, coordinate);
            renderRoster(roster);
            renderArmy(roster.player.army);
        });
    });
}
var armySelected = null;
function armyDeselectAll() {
    armySelected = null;
    $('army > character.selected').removeClass('selected');
}
function armySelect(uniqueName) {
    var $node = $('#army_' + uniqueName);
    if($node.parents(".disabled").length > 0) return;
    armyDeselectAll();
    armySelected = uniqueName;
    $('#army_' + uniqueName).addClass('selected');
    renderRoster(window.player.roster,true);//TODO: Check if this is a acceptable practice and suggest better alternative
}

function getIcon(characterName)
{
    switch(characterName)
    {
        case "HeroesArcherClass":  return ":fishing_pole_and_fish:"; break;
        case "HeroesMageClass":  return ":tophat:"; break;
        case "HeroesWarriorClass":  return ":skull:"; break;
        case "HeroesDragonClass": return ":dragon:"; break;
        default: return ":man:"; break;
    }
}

function renderArmy(army) {
    var html = "<army>";
    army.characters.forEach(function(character){
        var className = character.canJoinBattle()?"":" disabled";
        var onclick = "";
        var icon = "";
        try {
            icon = emojione.shortnameToImage(getIcon(character.characterClass.constructor.name));
            icon = icon.replace('src="//','src="http://');
        } catch (e) {
        }
        if (army.player.roster.activeCharacters().indexOf(character) < 0) {
            className += " out-of-roster";
            onclick = "onClick='armySelect(\"" + character.uniqueName + "\");'"
        } else {
            className += " active";
        }
        html += "<character id='army_" + character.uniqueName + "' class='"+ className + "' " + onclick + " >"
        + character.getShortName()
        + " <experience>" + character.experienceRequiredToLevelUp() + "</experience>"
        + " <icon>"+  icon +"</icon>"
        + "<health><bar><state style='width:" + Math.floor( (character.health/character.maxHealth())*100 ) +"%;' /></bar><current>" + Math.round(character.health) + "</current><max>" + character.maxHealth() + "</max></health></character>";
    });


    html += "</army>";

    $("#army").html(html);
}

var rosterTopLeft = function(x, y) {
    var top = Math.round(y * rosterTileHeight);
    var left = Math.round((x + ((y+1) % 2)/2.0) * rosterTileWidth);
    return [top, left];
}
