var radius = 30;
var tileWidth = Math.sqrt(3)*radius;
var tileHeight = radius*2;

var setupDemoBattle = function() {
    var r = demoRosterForNodeId();
    var b = new Board(null, 20, 13);
    b.addPlayerAndCharactersFromRoster(r);
    addPlayerRosterToBoard(b);

    return b;
}

var addPlayerRosterToBoard = function(b) {
    var felipe = new Player("player");
    b.addPlayer(felipe, 0);

    for (var i = 0; i <= 4; i += 2) {
        b.addCharacter(felipe, new Character(HeroesMageClass, new HexCoordinate(0 + i % 3, 6)));
        b.addCharacter(felipe, new Character(HeroesWarriorClass, new HexCoordinate(2 + i % 3, 5)));
        b.addCharacter(felipe, new Character(HeroesArcherClass, new HexCoordinate(1 + i % 3, 1)));
    }
}

function renderBattle(board) {
    var html = "<board>";
    html += "<svg height=\"" + Math.ceil(board.height * tileHeight) + "\" width=\"" + Math.ceil((0.5+board.width) * tileWidth) + "\">";

    for (var y = 0; y < board.height; y++) {
        for (var x = 0; x < board.width; x++) {
            var top_left = topLeft(x, y);
            var location = 'top:' + top_left[0] + 'px;left:' + top_left[1] + 'px;';
            var t = board.terrainAtCoordinate(new HexCoordinate(x, y));

            var p1 = (top_left[1]) + "," + (top_left[0] + 1 * radius/2);
            var p2 = (top_left[1] + Math.sqrt(3)*radius/2) + "," + (top_left[0]);
            var p3 = (top_left[1] + Math.sqrt(3)*radius) + "," + (top_left[0] + 1 * radius/2);
            var p4 = (top_left[1] + Math.sqrt(3)*radius) + "," + (top_left[0] + 3 * radius/2);
            var p5 = (top_left[1] + Math.sqrt(3)*radius/2) + "," + (top_left[0] + 4 * radius/2);
            var p6 = (top_left[1]) + "," + (top_left[0] + 3 * radius/2);

            let tile = "<polyline class=\""+ t + "\" style=\"stroke-width:1;stroke:rgb(0,0,0)\" points=\"{0} {1} {2} {3} {4} {5} {6}\"/>".format(p1, p2, p3, p4, p5, p6, p1);
            html += tile;
        }
    }
    html += "</svg>";
    board.characters.forEach(function(character){
        var icon = "";
        try {
            icon = emojione.shortnameToImage(getIcon(character.characterClass.constructor.name));
            icon = icon.replace('src="//','src="http://');
        } catch (e) {
        }
        html += "<character class='hexagon " + character.player.name + "' id='" + character.uniqueName + "'>"
        + " <icon>"+  icon +"</icon>"
        + "<health/>"
        + "</character>";
    });

    html += "</board>";

    $("#board").html(html)
    .css({height:board.height*tileHeight});
    $("#board board").css({width:board.width*tileWidth,height:board.height*tileHeight});
}


var topLeft = function(x, y) {
    var top = Math.round(y * tileHeight * 3 / 4);
    var left = Math.round((x + ((y+1) % 2)/2.0) * tileWidth);
    return [top, left];
}
var updateCharacters = function(board, only_character) {
    board.characters.forEach(function(character){
        var top_left = topLeft(character.coordinate.x, character.coordinate.y);
        $('#' + character.uniqueName).css({
            opacity:(character.isAlive()?1:0.2),
            zIndex:(character.isAlive()?2:1)
        });
        if (!only_character || only_character == character) {
            $('#' + character.uniqueName).css({
                top: top_left[0] + 'px',
                left: top_left[1] + 'px',
                position: 'absolute'
            });
        }
        $('#' + character.uniqueName + ' > health').text(Math.round(character.health));
    });
};
var animateCharacter = function(character, steps, attacks) {
    var animateStep = function() {
        if (steps && steps.length > 0) {
            var step = steps.shift();
            if (step) {
                var top_left = topLeft(step.x, step.y);
                $('#' + character.uniqueName).css({
                    top: top_left[0] + 'px',
                    left: top_left[1] + 'px',
                    position: 'absolute'
                });
            }
            setTimeout(animateStep, (steps.length > 0?50:100));
        } else {
            if (attacks && attacks.length > 0) {
                var top_left_c = topLeft(character.coordinate.x, character.coordinate.y);
                attacks.forEach(function(attack) {
                    var top_left_t = topLeft(attack.target.coordinate.x, attack.target.coordinate.y);
                    var top_left_dif = [top_left_t[0] - top_left_c[0], top_left_t[1] - top_left_c[1]];
                    var top_left_d = Math.sqrt(top_left_dif[0] * top_left_dif[0] + top_left_dif[1] * top_left_dif[1]);

                    top_left_c[0] += (tileWidth / 2) * top_left_dif[0] / top_left_d;
                    top_left_c[1] += (tileHeight / 2) * top_left_dif[1] / top_left_d;

                    $('#' + character.uniqueName).css({
                        top: top_left_c[0] + 'px',
                        left: top_left_c[1] + 'px',
                        position: 'absolute'
                    });
                    $('#' + attack.target.uniqueName).css({
                        opacity: 0.5,
                    });
                });
                setTimeout(animateStep, 100 * attacks.length);
                attacks = null;
            } else {
                updateCharacters(character.board, character);
            }
        }
    };
    animateStep();
};

var battleAutoplay = false;
var nextTurn = function(board) {
    var play = board.nextCharacterMovesAndAttacks();
    animateCharacter(play.character, play.path.steps, play.attackResults);

    if (battleAutoplay) {
        setTimeout(function () { if (!board.isOver()) { nextTurn(board); } }, battleAutoplay);
    }
};
