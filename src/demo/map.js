
var tileSide = 40;

var demoRosterForNodeId = function() {
    let jsonUrl = "/static/rosters/A.json";
    return Roster.rosterWithURL(jsonUrl);
}

var setupDemoMap = function(player) {
    let jsonUrl = "/static/maps/A.json";
    let map = Map.mapWithURL(jsonUrl);
    map.setPlayer(player);
    return map;
}

function renderPlayer(player) {
    $("#player").html("<name>{0}</name><resource class='gold'>{1}</resource>".format(player.name, player.getResourceAmountForType(kResourceGold)));
}

function renderMap(map) {
    var zoom = 3;
    var html = "<heroesMap style='width:" + tileSide * map.width + "px;height:" + tileSide * map.height + "px;'/>";
    $("#map").html(html);
    map.links.forEach(function(link){
        link.tiles(zoom).forEach(function(tile) {
            var top_left = squareTopLeft(tile.x, tile.y);
            var style = 'top:' + top_left[0] + 'px;left:' + top_left[1] + 'px;';
            var className = '';
            if (link.isAccessible()) {
                className = 'accessible';
            } else {
                className = 'unaccessible';
            }
            var id = 'link_' + tile.toJString();
            $("#map").append("<heroesLink id='" + id + "' class='" + className + "' style='" + style + "'></heroesLink>");
        });
    });
    map.nodes.forEach(function(node){
        var top_left = squareTopLeft(node.coordinate.x * zoom, node.coordinate.y * zoom);
        var style = 'top:' + top_left[0] + 'px;left:' + top_left[1] + 'px;';
        var className = '';
        if (node.isUnlocked()) {
            className = 'unlocked';
        } else {
            if (node.isAccessible()) {
                className = 'accessible';
            } else {
                className = 'unaccessible';
            }
        }
        var id = 'node_' + node.coordinate.toJString();
        var reward = "";
        var w = node.waitUntilCanRewardPlayer();
        if (w !== null && w > 0) {
            reward = "<br>" + Math.round(w);
        }

        var miniroster = ['<tooltip>'];
        miniroster.push('<resources>');
        node.resourceRewardsWithTime().forEach(function(resource) {
            miniroster.push('<resource class="resource_' + resource.type + '">' + resource.type[0] + resource.amount.toFixed(1) + '</resource>');
        });
        miniroster.push('</resources>');
        miniroster.push('<army class="mini">');
        node.getRosterDescriptor().characters.forEach(function(character){
            var icon = "";
            try {
                icon = emojione.shortnameToImage(getIcon(character.characterClass.constructor.name));
                icon = icon.replace('src="//','src="http://');
            } catch (e) {
            }

            miniroster.push(
                "<character id='mini_"+character.uniqueName+"'>"
                + " <icon>" +  icon + "</icon>"
                + "</character>"
            );
        });
        miniroster.push('</army>');
        miniroster.push('</tooltip>');

        $("#map").append("<heroesnode id='" + id +"' class='" + className + "' style='" + style + "'>" + node.id + reward + miniroster.join("") + "</heroesnode>");
        var nodeId = node.id;
        $("#" + id).click(function() {
            if (node.isAccessible()) {
                map.startBattle(map, nodeId, true);
            }
        });

    });
};
var squareTopLeft = function(x, y) {
    var top = Math.round(y * tileSide);
    var left = Math.round(x * tileSide);
    return [top, left];
};
